/*
 * dbPool.h
 *
 *  Created on: Feb 27, 2017
 *      Author: dinesh
 */

#ifndef INCLUDE_DBPOOL_H_
#define INCLUDE_DBPOOL_H_

#include "dbpref.h"
#include <unistd.h>

class connPooler
{
    private:
        vector<postgres *> *conPool;
        mutex poolLock;
        int dbPoolCount;
    public:
        connPooler();
        connPooler (postgres *con, int cnt);
        postgres *getConnFromPool();
        void pushConnIntoPool (postgres *con);
        void clearPool();
        int getPoolCount() const;
        int getActiveCount();
        int getAvailableCount();
        string printConnPoolState (bool isExtended, uint numcols);

        // Wrapper functions around the actual libpq function calls.
        //
        vector<string> *execGet1DArray (string, string &);
        vector<vector<string>> execGet2DArray (string, std::unordered_map<string, size_t> &, string &emsg);
        string execGetScalar (string, string &);
        void execStmt (string, string &);
        string execGetCSV (string, string &err, pid_t &pid);
        string escapeLiteral (string input);		// This method escape quotes, backslash, ...
        bool execDMLstmt (string, string &);

        ~connPooler();
};

#endif /* INCLUDE_DBPOOL_H_ */
