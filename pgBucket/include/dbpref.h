/*
 * pgBucket database objects declaration
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/include/dbpref.h
 */

#ifndef INCLUDE_DBPREF_H_
#define INCLUDE_DBPREF_H_

#include <unordered_map>
#include <errno.h>
#include "utils/utils.h"
#include "libpq-fe.h"


enum DbTypes { POSTGRESQL, NOTSUPPORTED };

class dbPref
{
    private:
        string	dbUserName;
        string	dbMd5Password;
        string	dbHostIP;
        DbTypes dbType;
        size_t	dbPort;
    public:
        dbPref();
        dbPref (string dbuser, string password, string hostip, DbTypes dbtype, size_t dbport);
        string getDbUserName();
        string getDbMd5Password();
        string getDbHostIp();
        DbTypes getDbType();
        size_t getDbPort();
        size_t getDbVersion();
};

class postgres : public dbPref
{
    private:
        // JobId, Query
        //
        PGconn *pgDbConn;
        string jobDbConnStr;
        string dbName;
        size_t 	dbVersion;
    public:
        postgres();
        postgres (string jobDbConn);
        postgres (const string &dbuser, const string &password, const string &hostip, const string &dbname, DbTypes dbtype, size_t dbport);
        void closeConnection();
        PGconn *getPgConn();
        void setPgConn (PGconn *con);
        PGconn *connectAndGetDBconn();
        vector<string> *execGet1DArray (string, string &);
        vector<vector<string>> execGet2DArray (string, std::unordered_map<string, size_t> &, string &);
        string execGetScalar (string, string &);
        void execStmt (string, string &);
        string execGetCSV (string, string &err, pid_t &pid, bool isStoreColNames = false);
        string execRunGetScalar (string query, string &emsg, pid_t &pid);
        string escapeLiteral (string input);		// This method escape quotes, backslash, ...
        pid_t getDbPid();
        bool execDMLstmt (string, string &);
        bool beginTrx (string &);
        bool endTrx (string &);
        bool chkExecStatus (PGresult *res, PGconn *conn, string &emsg, bool isTupReturn = false);
        string getDbName() const;
        ~postgres();
};


#endif
