﻿/*
 * schDbOps.cpp
 *
 *      This source file has the implementation of scheduler related database operations.
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/source/schDbOps.cpp
 */

#include "../include/schDbOps.h"
#include "../include/jobConfig.h"
extern configParams *settings;
connPooler *schDbOps::dbConPool;

string schDbOps::catalog = "\
CREATE SCHEMA _schedule_pgbucket_; \n\
CREATE TABLE _schedule_pgbucket_.jobs(\n\
		jobid                                   int primary key, \n\
		jobname                                 text, \n\
		jobcreation                             timestamp default now(), \n\
		jobupdation                             timestamp default null,\n\
		jobstatus                               bool, \n\
		jobType                                 char(2), \n\
		jobRunFreq                              text, \n\
		jobClass                                char(3),\n\
        jobFailEventIds                         int[],\n\
        jobPassEventIds                         int[],\n\
        jobDisableIfFailCnt                     int,\n\
        jobParseCmdParams                       bool,\n\
        jobRecordDbResCols                      bool,\n\
        jobFailIfResult                         text\n\
);\n\
CREATE TABLE _schedule_pgbucket_.jobstatus( \n\
		sno										bigserial, \n\
		jobid                                   int, \n\
		jobcmd									text, \n\
		jobname                                 text, \n\
		jobstarttime                            timestamp default now(), \n\
		jobendtime                              timestamp, \n\
		status                                  char(1),    \n\
		result                                  text,        \n\
		error                                   text, \n\
		jobnextrun                              timestamp, \n\
		jobpid                                  int, \n\
		execpath                                text\n\
);\n\
CREATE TABLE _schedule_pgbucket_.jobcmd(\n\
		jobid                                   int REFERENCES _schedule_pgbucket_.jobs(jobid) ON DELETE CASCADE,\n\
		cmd                                     text, \n\
		scriptfile                              text\n\
);\n\
CREATE TABLE _schedule_pgbucket_.jobdbcred(\n\
		jobid                                   int REFERENCES _schedule_pgbucket_.jobs(jobid) ON DELETE CASCADE,\n\
		dbcreds                                 text\n\
);\n\
CREATE TABLE _schedule_pgbucket_.jobrunfreq(\n\
		jobid                                   int REFERENCES _schedule_pgbucket_.jobs(jobid) ON DELETE CASCADE,\n\
		days                                    text,\n\
		months                                  text,\n\
		hours                                   text,\n\
		minutes                                 text,\n\
		seconds                                 text\n\
);\n\
CREATE TABLE _schedule_pgbucket_.jobsynchash(\n\
		seqid                                   serial,\n\
		jobid                                   int,\n\
		jobClass                                char(3),\n\
		syncop                                  varchar\n\
);";

/*
 * initCatalog:
 *      This method initialize the catalog tables.
 */
void schDbOps::initCatalog()
{
    try {
        string emsg;
        dbConPool->execStmt (catalog, emsg);

    } catch (exception &e) {
        throw;
    }
}
/*
 * dropCatalog:
 *      This method drop the pgbucket catalog schema.
 */
void schDbOps::dropCatalog()
{
    try {
        string emsg;
        dbConPool->execStmt ("DROP SCHEMA _schedule_pgbucket_ CASCADE;", emsg);

    } catch (exception &e) {
        throw;
    }
}
/*
 * schDbOps:
 *      Parameterized default constructor.
 */
schDbOps::schDbOps (connPooler *cPool, hashTable *jH, hashTable *eH)
{
    dbConPool = cPool;
    jHash = jH;
    eHash = eH;
    jobIds = nullptr;
}
/*
 * delJob:
 *      This method drop the given job id from the database.
 */
bool schDbOps::delJob (string &jId)
{
    // If job id is empty, then return from here..
    //
    if (jId == "") {
        printMsg ("Found an invalid jobid while updating the job..");
        return false;
    }

    try {
        string emsg;
        dbConPool->execGet1DArray ("DELETE FROM _schedule_pgbucket_.jobs WHERE jobid = " + jId + " RETURNING jobid", emsg);

        // If daemon is running, then insert an entry into jobsynchash table
        //
        if (isPgBucketRunning())
            dbConPool->execDMLstmt ("INSERT INTO _schedule_pgbucket_.jobsynchash(jobid, syncop) VALUES(" + jId + ",'D')", emsg);

        return true;

    } catch (exception &e) {
        throw;
    }
}
/*
 * insUpdJob:
 *      This method insert/update the given job configuration details in database.
 */
bool schDbOps::insUpdJob (string &jobid, string &jobname, string &jobstatus,
                          string &jobType, string &jobRunFreq, string &days, string &months, string &hours,
                          string &minutes, string &seconds, string &cmd, string &dbConn,
                          string &disableFailCnt, string &isParseCmd, string &isStoreDbCols,
                          string &jobClass, string &jobPEjids, string &jobFEjids, string &jobFailIfOut, bool isUpdate)
{
    string qry, emsg;

    try {
        //TODO:
        // Think one more time before removing the below scopeExit functionality.
        // OR, keep the below code as a refferal for "How to implement GOLANG defer functionality in C++11.
        //
        //
        //        auto scopeExit = [] (postgres* con) {
        //            LOG("Pushing connection back to db pool", DDEBUG);
        //            dbConPool->pushConnIntoPool(con);
        //        };
        //        std::unique_ptr<postgres, decltype(scopeExit)> con(
        //            dbConPool->getConnFromPool(),
        //            scopeExit
        //        );
        // Inserting a new job into the pgBucket database
        //
        if (!isUpdate) {
            // Escape quotes in the jobname
            //
            qry = "INSERT INTO _schedule_pgbucket_.jobs(jobid, jobname, jobstatus, jobType, jobClass," \
                  "jobRunFreq, jobDisableIfFailCnt, jobParseCmdParams, jobRecordDbResCols, jobFailEventIds, jobPassEventIds, jobFailIfResult) " \
                  "VALUES ( " + jobid + ", " +
                  dbConPool->escapeLiteral (jobname) + ", " + toQuote (jobstatus) + ", " + toQuote (jobType) + ", " + toQuote (jobClass)
                  + ", " + toQuote (jobRunFreq) + ", " + (disableFailCnt.empty() ? "0" : disableFailCnt) + ", " +
                  (isParseCmd.empty() ? "false" : isParseCmd) + "," + (isStoreDbCols.empty() ? "false" : isStoreDbCols) + ",";

            qry += jobFEjids.empty() ? " array[]::integer[], " : " array[" + jobFEjids + "], ";
            qry += jobPEjids.empty() ? " array[]::integer[], " : " array[" + jobPEjids + "], ";
            qry += toQuote (jobFailIfOut) + ");";

            dbConPool->execDMLstmt (qry, emsg);
            dbConPool->execDMLstmt ("INSERT INTO _schedule_pgbucket_.jobcmd(jobid, cmd) VALUES(" + jobid + ", " + dbConPool->escapeLiteral (cmd) + ");", emsg);

            qry = "INSERT INTO _schedule_pgbucket_.jobrunfreq(jobid, days, months, hours, minutes, seconds) VALUES("
                  + jobid + ", " + toQuote (days) + ", " + toQuote (months) + ", " + toQuote (hours) + ", " + toQuote (minutes) + ", " + toQuote (seconds) + ");" ;

            dbConPool->execDMLstmt (qry, emsg);

            // If dbConn property is empty for the job, then ignore.
            // For OS related jobs, we don't require dbconn property.
            //
            if (!dbConn.empty()) {
                qry = "INSERT INTO _schedule_pgbucket_.jobdbcred(jobid, dbcreds) VALUES(" + jobid + ", " + toQuote (dbConn) + ");";
                dbConPool->execDMLstmt (qry, emsg);
            }

            // If daemon is running, make an entry into jobsynchash table.
            //
            if (isPgBucketRunning()) {
                qry = "INSERT INTO _schedule_pgbucket_.jobsynchash(jobid, syncop, jobClass) VALUES(" + jobid + ",'I', " + jobClass + ")";
                dbConPool->execDMLstmt (qry, emsg);
            }

            return true;
        }

        // Updating an existing job with new properties in the pgBucket database.
        //
        else {
            if (jobid == "")
                throw schExcp (ERROR, "Found an invalid job id while updating the job..");

            qry = "UPDATE _schedule_pgbucket_.jobs SET jobname=" + toQuote (jobname) + ", jobstatus= " + toQuote (jobstatus) +
                  ", jobType = " + toQuote (jobType) + ", jobRunFreq = " + toQuote (jobRunFreq)  +
                  ", jobClass = " + toQuote (jobClass) + ", jobDisableIfFailCnt = " + (disableFailCnt.empty() ? "0" : disableFailCnt) +
                  ", jobParseCmdParams = " + (isParseCmd.empty() ? "false" : isParseCmd) + ", jobRecordDbResCols = " + (isStoreDbCols.empty() ? "false" : isStoreDbCols) + ", ";


            // If the set of job fail/pass ids are empty then push an empty array into the database
            //
            qry += jobFEjids.empty() ? " jobFailEventIds = array[]::integer[], " : " jobFailEventIds = string_to_array(" + toQuote (jobFEjids) + ", ',')::integer[],";
            qry += jobPEjids.empty() ? " jobPassEventIds = array[]::integer[], " : " jobPassEventIds = string_to_array(" + toQuote (jobPEjids) + ", ',')::integer[],";

            qry += " jobFailIfResult= " + toQuote (jobFailIfOut) + ", jobupdation = now() WHERE jobid = " + jobid + " RETURNING jobid";


            // If the given jobid not found in the database, then return from here..
            //
            if (dbConPool->execGet1DArray (qry, emsg) == nullptr)
                throw schExcp (ERROR, "Jobid not found in database: " + jobid);

            // Updating job's run command
            //
            qry = "UPDATE _schedule_pgbucket_.jobcmd SET cmd= " + dbConPool->escapeLiteral (cmd) +
                  " WHERE jobid = " + jobid + ";" ;

            if (!dbConPool->execDMLstmt (qry, emsg))
                throw schExcp (ERROR, emsg);

            // Updating jobs' run frequency
            //
            qry = "UPDATE _schedule_pgbucket_.jobrunfreq SET months= " + toQuote (months) + ", days = " + toQuote (days) +
                  ", hours = " + toQuote (hours) + ", minutes = " + toQuote (minutes) + ", seconds = " + toQuote (seconds) +
                  " WHERE jobid = " + jobid + ";" ;

            if (!dbConPool->execDMLstmt (qry, emsg))
                throw schExcp (ERROR, emsg);

            // Updating job's database login credentials
            //
            qry = "UPDATE _schedule_pgbucket_.jobdbcred SET dbcreds= " + toQuote (dbConn) + " WHERE jobid = " + jobid + ";";

            if (!dbConPool->execDMLstmt (qry, emsg))
                throw schExcp (ERROR, emsg);

            if (isPgBucketRunning()) {
                // If the daemon is running, then make an entry into,
                // syncHash table as "Delete" and "Insert" for this job.
                //
                qry = "INSERT INTO _schedule_pgbucket_.jobsynchash(jobid, syncop, jobclass) VALUES(" + jobid + ",'UD'," + jobClass + ")";

                if (!dbConPool->execDMLstmt (qry, emsg))
                    throw schExcp (ERROR, emsg);

                qry = "INSERT INTO _schedule_pgbucket_.jobsynchash(jobid, syncop, jobclass) VALUES(" + jobid + ",'UI'," + jobClass + ")";

                if (!dbConPool->execDMLstmt (qry, emsg))
                    throw schExcp (ERROR, emsg);
            }

            return true;
        }

    } catch (exception &e) {
        e.what();
        throw schExcp (ERROR, "Insert/Update of the Job id ->" + jobid + " is failed..");
    }
}
/*
 * loadJobIds:
 *      This method load the list of job ids into the jobIds member variable
 */
void schDbOps::loadJobIds()
{
    string emsg;

    // If job ids are not empty, then clear them and load fresh job ids into the vector.
    //
    try {
        if (jobIds != nullptr)
            jobIds->clear();

        jobIds = dbConPool->execGet1DArray ("SELECT jobid FROM _schedule_pgbucket_.jobs", emsg);

    } catch (exception &e) {
        throw;
    }
}
/*
 * isJobIdExists:
 *      This method validate whether the job already exists or not
 */
bool schDbOps::isJobIdExists (string jobId)
{
    if (jobIds == nullptr)
        return false;

    if (jobIds->size() == 0) {
        return false;

    } else {
        sort (jobIds->begin(), jobIds->end());

        if (binary_search (jobIds->begin(), jobIds->end(), jobId)) {
            return true;

        } else
            return false;
    }
}
/*
 * printStatJobs:
 *      This method print the jobs status.
 */
void schDbOps::printStatJobs (char &status, string &nJobs, string &jobId, bool isExtended, uint numcols)
{
    int val;

    if (! (status == 'S' || status == 'F' || status == 'A' || status == 'R' || status == 'E' || status == 'D')) {
        printMsg ("Invalid status option. Allowed values are D, A, R, E, S, F");
        return;

    } else if (!string2Int (nJobs, val, -1)) {
        return;
    }

    string limit = " LIMIT " + ((nJobs == "-1") ? "ALL" : nJobs);
    string baseQy;
    string where = " WHERE true=true " + ((jobId != "") ? " AND j.jobid = " + jobId : "");
    string tableHeader;

    if (status == 'E' || status == 'D') {
        baseQy = "SELECT j.jobid as \"ID\", j.jobname as \"Name\","  \
                 "j.jobtype as \"Type\", " \
                 "trim(jc.cmd, E'\n') as \"Command\"," \
                 "(CASE WHEN j.jobclass = 'EVT' THEN 'NONE' " \
                 "ELSE 'Day: '||jr.days||' Mon: '||jr.months||' H: '||jr.hours||' M: '||jr.minutes||' S: '||jr.seconds END) as \"Run Frequency\", " \
                 "j.jobclass as \"Class\", " \
                 "j.jobfaileventids as \"FailEventIds\", "\
                 "j.jobpasseventids as \"PassEventIds\", "\
                 "j.jobDisableIfFailCnt as \"DisableFailCnt\", "\
                 "j.jobfailifresult as \"FailIfResult\", "\
                 "j.jobParseCmdParams as \"PraseCmd\", "\
                 "j.jobRecordDbResCols as \"RecordColNames\" "\
                 " FROM _schedule_pgbucket_.jobs j INNER JOIN _schedule_pgbucket_.jobcmd jc ON jc.jobid=j.jobid "\
                 " INNER JOIN _schedule_pgbucket_.jobrunfreq jr ON jr.jobid=jc.jobid ";

        if (status == 'E') {
            where += "AND j.jobstatus IS TRUE";
            tableHeader = "Enabled ";
        }

        if (status == 'D') {
            where += "AND j.jobstatus IS NOT TRUE";
            tableHeader = "Disabled ";
        }

        baseQy += where + limit;

    } else {
        baseQy = "SELECT js.sno as \"Serial ID\",js.jobid as \"ID\", js.jobname as \"Name\"," 	\
                 "trim(js.jobcmd, E'\n') as \"Command\", date_trunc('seconds', js.jobstarttime) as \"Start Time\"," \
                 "date_trunc('seconds', js.jobendtime) as \"End Time\", date_trunc('seconds',js.jobendtime-js.jobstarttime) as \"Duration\"," \
                 "js.jobpid as \"PID\", (CASE WHEN js.status = 'S' THEN coalesce(date_trunc('seconds', js.jobnextrun)::text, 'No more schedules today') "\
                 "ELSE date_trunc('seconds', js.jobnextrun)::text END) as \"Next Run\", js.execpath as \"Exec Path\", " ;

        if (status == 'S') {		// Success jobs
            baseQy += "'Success' as \"JobStatus\" ";
            where += " AND js.status = 'S' ";
            tableHeader = "Successful ";

        } else if (status == 'R') {	// Running jobs
            baseQy += "'Running' as \"JobStatus\" ";
            where += " AND js.status = 'R' ";
            tableHeader = "Running ";

        } else if (status == 'F') {	// Failed jobs
            baseQy += "'Failed' as \"JobStatus\", error as \"Error\" ";
            where += " AND js.status = 'E' ";
            tableHeader = "Failed ";

        } else if (status == 'A') {	// All jobs
            baseQy += "(CASE WHEN js.status='R' then 'Running' " \
                      "WHEN js.status='E' then 'Failed' " \
                      "WHEN js.status='S' then 'Success' END ) as \"JobStatus\", trim(error,E'\n') as \"Error\" ";
            tableHeader = "All ";
        }

        baseQy = baseQy + " FROM _schedule_pgbucket_.jobs j RIGHT JOIN _schedule_pgbucket_.jobstatus js ON j.jobid=js.jobid LEFT JOIN _schedule_pgbucket_.jobcmd jc ON j.jobid=jc.jobid "
                 + where + " ORDER BY js.jobendtime DESC NULLS LAST, js.jobstarttime DESC NULLS LAST " + limit;
    }

    try {
        std::unordered_map<string, size_t> fields;
        vector<vector<string>> results;
        string emsg;
        results = dbConPool->execGet2DArray (baseQy, fields, emsg);
        vector<string> oFields (fields.size());

        // Fetching only fields, from the assoc map
        //
        for (auto &x : fields)
            oFields.at (x.second) = x.first;

        std::cout << printTable (tableHeader + " Jobs", results, oFields, isExtended, numcols);

    } catch (exception &e) {
        throw;
    }
}
/*
 * printRunIdStats:
 *      This method print the job results for the given run id.
 */
void schDbOps::printRunIdStats (string &runid, bool isExtended, uint numcols)
{
    long seqId;
    std::unordered_map<string, size_t> fields;
    vector<vector<string>> results;

    try {
        // Validate the run id, and check whether it is a valid number or not.
        //
        string2Long (runid, seqId);

        string emsg;
        results = dbConPool->execGet2DArray ("SELECT sno \"Serial id\", jobid \"ID\", trim(jobcmd, E'\n') \"Command\", trim(result, E'\n') \"Result\", execpath \"Exec Path\" FROM _schedule_pgbucket_.jobstatus WHERE sno=" + runid, fields, emsg);
        vector<string> oFields (fields.size());

        // Pushing column names into oFields vector, by reading fields map
        //
        for (auto &x : fields)
            oFields.at (x.second) = x.first;

        std::cout << printTable ("Serial ID " + runid + " result", results, oFields, isExtended, numcols);

    } catch (exception &e) {
        throw;
    }
}
/*
 * getJobHash:
 *      Getter method for the jHash member variable.
 */
hashTable *schDbOps::getJobHash()
{
    return jHash;
}

hashTable *schDbOps::getEventHash()
{
    return eHash;
}

hashTable *schDbOps::pushEvnts2Hash (size_t jobId)
{
    string getEvntsQy = " SELECT sb.jobid, jobname, jobtype, jobClass, array_to_string(jobFailEventIds, ',') jobFailEventIds," \
                        " array_to_string(jobPassEventIds,',') jobPassEventIds, jobDisableIfFailCnt, jobFailIfResult, sb.jobstatus," \
                        " jobRecordDbResCols, jobParseCmdParams, "
                        " cmd, dbcreds " \
                        " FROM _schedule_pgbucket_.jobs sb INNER JOIN _schedule_pgbucket_.jobcmd sjc" \
                        " ON sb.jobid = sjc.jobid " \
                        " LEFT JOIN _schedule_pgbucket_.jobdbcred sjdb "\
                        " ON sjc.jobid = sjdb.jobid "\
                        " WHERE LOWER(sb.jobclass) = 'evt'";
    vector<vector<string>> result;
    std::unordered_map<string, size_t> fields;
    string emsg;

    // Got the new job, push this to Hash
    //
    if (jobId != 0) {
        getEvntsQy += " AND sb.jobid = " + std::to_string (jobId);
    }

    try {
        result = dbConPool->execGet2DArray (getEvntsQy, fields, emsg);

        if (result.size() == 0) {
            LOG ("No event jobs found to push into the pgBucket's event hash", NOTICE);
            return eHash;
        }

        for (auto row = result.begin(); row != result.end(); ++row) {
            size_t *pejids, *fejids;
            pejids = parseEventJids (row->at (fields["jobpasseventids"]));
            fejids = parseEventJids (row->at (fields["jobfaileventids"]));
            int tmpCnt = 0;
            string2Int (row->at (fields["jobdisableiffailcnt"]), tmpCnt, 0);

            if (row->at (fields["jobtype"]) == "OS")
                eHash->insertJob (new osJobs ((size_t)stoi (row->at (fields["jobid"])), row->at (fields["jobname"]),
                                              (row->at (fields["jobstatus"])) == "t" ? true : false,
                                              nullptr, nullptr, nullptr,
                                              OSLEVEL,
                                              row->at (fields["cmd"]),
                                              (settings->getConfigParam ("child_process_mode")->compare ("fork_exec") == 0) ? FORK_EXEC : POSIX_SPAWN,
                                              row->at (fields["dbcreds"]),
                                              EVENT,
                                              pejids, fejids,
                                              row->at (fields["jobfailifresult"]),
                                              tmpCnt,
                                              row->at (fields["jobrecorddbresCols"]) == "t" ? true : false,
                                              row->at (fields["jobparsecmdparams"]) == "t" ? true : false
                                             )
                                 );
            else
                eHash->insertJob (new dbJobs ((size_t)stoi (row->at (fields["jobid"])), row->at (fields["jobname"]),
                                              (row->at (fields["jobstatus"])) == "t" ? true : false,
                                              nullptr, nullptr, nullptr,
                                              DBLEVEL,
                                              row->at (fields["cmd"]),
                                              row->at (fields["dbcreds"]),
                                              EVENT,
                                              pejids, fejids,
                                              row->at (fields["jobfailifresult"]),
                                              tmpCnt,
                                              row->at (fields["jobrecorddbresCols"]) == "t" ? true : false,
                                              row->at (fields["jobparsecmdparams"]) == "t" ? true : false
                                             )
                                 );
        }

        // Return the populated jobHash table
        //
        return eHash;

    } catch (exception &e) {
        throw;
    }
}
/*
 * pushJobs2Hash:
 *      This method push the jobs from database into hash.
 */
hashTable *schDbOps::pushJobs2Hash (size_t jobId)
{
    string getJobsQy = " SELECT sb.jobid, jobname, jobtype, jobClass, array_to_string(jobFailEventIds, ',') jobFailEventIds, " \
                       " array_to_string(jobPassEventIds, ',') jobPassEventIds, jobDisableIfFailCnt, " \
                       " jobRecordDbResCols, jobParseCmdParams," \
                       " jobFailIfResult, sb.jobstatus," \
                       " months, days, hours, minutes, seconds, cmd, dbcreds " \
                       " FROM _schedule_pgbucket_.jobs sb INNER JOIN _schedule_pgbucket_.jobcmd sjc" \
                       " ON sb.jobid = sjc.jobid " ;
    vector<vector<string>> result;
    std::unordered_map<string, size_t> fields;
    string emsg;

    // Got the new job, push this to Hash
    //
    if (jobId != 0) {
        getJobsQy += " AND sb.jobid = " + std::to_string (jobId);
    }

    getJobsQy += " INNER JOIN _schedule_pgbucket_.jobrunfreq sjf "\
                 " ON sjf.jobid = sjc.jobid " \
                 " LEFT JOIN _schedule_pgbucket_.jobdbcred sjdb "\
                 " ON sjc.jobid = sjdb.jobid " \
                 " WHERE LOWER(sb.jobclass) = 'job'";

    try {
        result = dbConPool->execGet2DArray (getJobsQy, fields, emsg);

        if (result.size() == 0) {
            LOG ("No jobs found to push into the pgBucket's job hash", NOTICE);
            return jHash;
        }

        for (auto row = result.begin(); row != result.end(); ++row) {
            bool isPush = false;
            vector<int> vals;

            // Check if this job is scheduled to run in current MONTH
            //
            if (row->at (fields["months"]) == "*") {
                isPush = true;

            } else {
                vals = parseIntCsv (row->at (fields["months"]), 1, 12);

                if (find (vals.begin(), vals.end(), getTimeElement (MONTH))  != vals.end())
                    isPush = true;
            }

            // Check if this job is scheduled to run on today
            //
            if (isPush && row->at (fields["days"]) == "*") {
                isPush = true;

            } else {
                vals = parseIntCsv (row->at (fields["days"]), 1, 31);

                if (isPush && (find (vals.begin(), vals.end(), getTimeElement (DAY)) != vals.end()))
                    isPush = true;
                else {
                    isPush = false;
                    LOG ("Job id ->" + row->at (fields["jobid"]) + " is not scheduled to run today.", DDEBUG);
                }
            }

            if (isPush == true) {
                // This job is configured to run today.
                // Let's push this job to jobHash.
                //
                vector<int> *hrs = new vector<int> (24);
                vector<int> *mns = new vector<int> (60);
                vector<int> *secs = new vector<int> (60);
                int n = 0;

                // Generating 0 to 23 hours
                //
                if (row->at (fields["hours"]) == "*")
                    generate (hrs->begin(), hrs->end(), [&n] {return n++;});
                else
                    parseCronExprAndAdjust (hrs, row->at (fields["hours"]), "hours", row->at (fields["jobid"]));

                // Generating 0 59 minutes
                //
                n = 0;

                if (row->at (fields["minutes"]) == "*")
                    generate (mns->begin(), mns->end(), [&n] { return n++;});
                else
                    parseCronExprAndAdjust (mns, row->at (fields["minutes"]), "minutes", row->at (fields["jobid"]));

                // Generating 0 to 59 seconds
                //
                n = 0;

                if (row->at (fields["seconds"]) == "*")
                    generate (secs->begin(), secs->end(), [&n] {return n++;});
                else
                    parseCronExprAndAdjust (secs, row->at (fields["seconds"]), "seconds", row->at (fields["jobid"]));

                size_t *pejids, *fejids;
                pejids = parseEventJids (row->at (fields["jobpasseventids"]));
                fejids = parseEventJids (row->at (fields["jobfaileventids"]));

                int tmpCnt = 0;

                string2Int (row->at (fields["jobdisableiffailcnt"]), tmpCnt, 0);

                if (row->at (fields["jobtype"]) == "OS")
                    jHash->insertJob (new osJobs ((size_t)stoi (row->at (fields["jobid"])), row->at (fields["jobname"]),
                                                  (row->at (fields["jobstatus"])) == "t" ? true : false,
                                                  hrs, mns, secs,
                                                  OSLEVEL,
                                                  row->at (fields["cmd"]),
                                                  (settings->getConfigParam ("child_process_mode")->compare ("fork_exec") == 0) ? FORK_EXEC : POSIX_SPAWN,
                                                  row->at (fields["dbcreds"]),
                                                  JOB,
                                                  pejids, fejids,
                                                  row->at (fields["jobfailifresult"]),
                                                  tmpCnt,
                                                  row->at (fields["jobrecorddbresCols"]) == "t" ? true : false,
                                                  row->at (fields["jobparsecmdparams"]) == "t" ? true : false
                                                 )
                                     );
                else
                    jHash->insertJob (new dbJobs ((size_t)stoi (row->at (fields["jobid"])), row->at (fields["jobname"]),
                                                  (row->at (fields["jobstatus"])) == "t" ? true : false,
                                                  hrs, mns, secs,
                                                  DBLEVEL,
                                                  row->at (fields["cmd"]),
                                                  row->at (fields["dbcreds"]),
                                                  JOB,
                                                  pejids, fejids,
                                                  row->at (fields["jobfailifresult"]),
                                                  tmpCnt,
                                                  row->at (fields["jobrecorddbresCols"]) == "t" ? true : false,
                                                  row->at (fields["jobparsecmdparams"]) == "t" ? true : false
                                                 )
                                     );

            } else
                continue;
        }

        // Return the populated jobHash table
        //
        return jHash;

    } catch (exception &e) {
        throw;
    }
}
/*
 * parseCronExprAndAdjust
 *      This method parse the cron expression and evaluate it.
 */
void schDbOps::parseCronExprAndAdjust (vector<int> *vec, string &configval, const string &element, string &jobid)
{
    int sections, frm, to;

    if (element != "hours") {
        frm = 0;
        to = 59;
        sections = 60;

    } else {
        frm = 0;
        to = 23;
        sections = 24;
    }

    if (configval.find ("/") == string::npos) {
        *vec = parseIntCsv (configval, frm, to);
        return;
    }

    int val = stoi (configval.substr (configval.find ("/") + 1));

    if (val == 0)
        // Found */n format, where n=0
        // If n value is 0, then it is an invalid cron expression.
        //
        throw schExcp (ERROR, "Found invalid cron expression " + configval + ", while loading jobid-> " + jobid + " into job hash");

    int n = 0;

    // If the value is divisible by the section, then we will get the 0th position during the calculations.
    // If the value is not divisible by the sections, then we need to add the 0th position explicitly.
    // To add an extra 0th position to the vector, we need to resize the vector size accordingly.
    //
    if (sections % val == 0)
        vec->resize (sections / val);
    else
        vec->resize (1 + (sections / val));

    // Generating a vector based on "n"
    //
    generate (vec->begin(), vec->end(), [&n, val, sections] {
        /*
         * While calculating the possible hours to schedule a job,
         * and if we reach the maximum sections limit, return 0.
         */
        if ((n + val) >= sections)
            return 0;
        else
            return (n = (n + val) % sections);
    });
    //Sort the secs vector as per */n format.
    //Since, we are doing modulo division, which we may get 0 as an end element in the vector
    //
    sort (vec->begin(), vec->end(), [] (int i, int j) {
        return i < j;
    });
}
/*
 * syncHashWithDb:
 *      This method sync the job hash with database
 */
void schDbOps::syncHashWithDb (vector<vector<string>> &result, std::unordered_map<string, size_t> &fields)
{
    string syncQry = "SELECT jobid, syncop, jobclass FROM _schedule_pgbucket_.jobsynchash ORDER BY seqid";
    string emsg;

    try {
        result = dbConPool->execGet2DArray (syncQry, fields, emsg);

        for (auto row = result.begin(); row != result.end(); row++) {
            if (row->at (fields["syncop"]) == "D") {
                // set job's delete flag
                //
                if (row->at (fields["jobclass"]) == "JOB")
                    jHash->deleteJob (stoi (row->at (fields["jobid"])));
                else
                    eHash->deleteJob (stoi (row->at (fields["jobid"])));

                LOG ("Updated the Job id ->" + row->at (fields["jobid"]) + " properties", NOTICE);

            } else if ((row->at (fields["syncop"]) == "I") || (row->at (fields["syncop"]) == "UI")) {
                // Insert new job
                //
                if (row->at (fields["jobclass"]) == "JOB")
                    this->pushJobs2Hash (stoi (row->at (fields["jobid"])));
                else
                    this->pushEvnts2Hash (stoi (row->at (fields["jobid"])));

                LOG ("Updated the Job id ->" + row->at (fields["jobid"]) + " properties", NOTICE);
            }
        }

    } catch (exception &e) {
        throw;
    }
}
/*
 * cleanSyncHashDbTable:
 *      This method clean the job sync hash table
 */
bool schDbOps::cleanSyncHashDbTable()
{
    string emsg;

    if (!dbConPool->execDMLstmt ("DELETE FROM _schedule_pgbucket_.jobsynchash", emsg))
        throw schExcp (ERROR, "Unable to clear the synch hash table. Error:" + emsg);

    return true;
}
/*
 * markStaleRunStatusAsFail:
 *      This method mark the old running job status as failed
 */
bool schDbOps::markStaleRunStatusAsFail()
{
    string emsg;

    if (!dbConPool->execDMLstmt ("UPDATE _schedule_pgbucket_.jobstatus SET status = 'E', error = 'pgBucket parent process is killed' WHERE status = 'R'", emsg))
        throw schExcp (ERROR, "Unable update stale running processes status. Error:" + emsg);

    return true;
}

jobClass schDbOps::parseJobClass (string &jclass)
{
    if (strcasecmp (jclass.c_str(), "EVT") == 0)
        return EVENT;
    else if (strcasecmp (jclass.c_str(), "JOB") == 0)
        return JOB;
    else
        return UNKNOWN_CLASS;
}

size_t *schDbOps::parseEventJids (string &jEvntIds)
{
    size_t *eventIds = nullptr;

    if (jEvntIds.empty())
        return eventIds;

    try {
        auto eIds = parseIntCsv (jEvntIds, 1, NORANGE, -1);
        int i = 0;
        // Create an array of size_t, and then populate all the int CSV values into the array
        //
        eventIds = new size_t[eIds.size() + 1];

        for (auto &x : eIds)
            eventIds[i++] = (size_t) x;

        // Here, 0 will be acting as the NULL element
        //
        eventIds[i] = 0;
        return eventIds;

    } catch (exception &e) {
        throw;
    }
}
