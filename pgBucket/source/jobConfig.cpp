﻿/*
 * jobConfig.cpp
 *
 *      This source file has the implementation which deals with configuration file.
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/source/jobConfig.cpp
 */
#include "../include/jobConfig.h"

/*
 * getKey:
 * 		Return key from keyValue object
 */
string keyValue::getKey()
{
    return key;
}
/*
 * getValue:
 * 		Return the value from keyValue object
 */
string keyValue::getValue()
{
    return value;
}
/*
 * convertStr2KV:
 * 		This method split the string with = as delimiter and assign values to key, value members
 */
void keyValue::convertStr2KV (string str)
{
    regex rge ("(\\w+)\\s*(=)\\s*(.*)", std::regex_constants::icase);
    smatch strMatch;
    regex_match (str, strMatch, rge);

    // key != empty and =(assignment) != empty and val != empty
    //
    if (strMatch[1] != "" && strMatch[2] != "" && strMatch[3] != "") {
        // Seems, we found the Key = Val matched string
        //
        key = strMatch[1];
        value = strMatch[3];

    } else {
        key = "";
        value = "";
    }
}
configParams::configParams (const char *file)
{
    conFile = file;
    openConfStream();
    jobSectionLinePos = -1;
}
/*
 * openConfStream:
 * 		Open the ifstream of the configuration file
 */
void configParams::openConfStream()
{
    conFileStream.open (conFile, ifstream::in);
}
/*
 * closeConfStream:
 * 		Close the ifstream of the configuration file
 */
void configParams::closeConfStream()
{
    if (conFileStream.is_open())
        conFileStream.close();
}
/*
 *
 */
/*
 * setConfigParam:
 *		Set the given config parameter value
 */
void configParams::setConfigParam (string param, string setting, bool overWrite)
{
    LckGaurd lock (lockConfig);
    auto result = confParams.insert (make_pair (param, setting));

    // If key is already exists, then remove it and insert it back.
    //
    if (overWrite && result.second == false) {
        LOG ("Parameter " + param + " is already exists. Hence, replacing the parameter value " + confParams.at (param) + " with new value " + setting, DDEBUG);
        confParams.erase (param);
        confParams.insert (make_pair (param, setting));
    }
}
/*
 * getConfigParam:
 * 		Get the configuration setting for the given parameter.
 */
const string *configParams::getConfigParam (const string &param)
{
    LckGaurd lock (lockConfig);

    try {
        return &confParams.at (param);

    } catch (exception &e) {
        LOG ("Unable to find the parameter " + param + " in the configuration map.", ERROR);
        e.what();
        return nullptr;
    }
}
/*
 * reloadSettings:
 * 		This method re-parse the configuration file, and reload the settings which can be reload
 */
bool configParams::reloadSettings()
{
    closeConfStream();
    openConfStream();

    if (parseConfig (true))
        return true;

    return false;
}
/*
 * parseConfig:
 * 		This method only parse the [CONFIG] section entries from the file,
 * 		and then update the config map.
 */
bool configParams::parseConfig (bool isReload)
{
    size_t lineNum = 1;
    bool isConfigSectionFound = false, isJobSectionFound = false;

    try {
        if (conFileStream.good()) {
            LOG ("Configuration parsing is started..", DDEBUG);

            for (string line; getline (conFileStream, line) && !isJobSectionFound; lineNum++) {
                keyValue kv;
                line = trim (line);

                // Checking whether line is empty
                //
                if (line.empty())
                    continue;

                // Checking if line is a comment
                //
                if (regex_search (line, regex ("^#|[[:space:]]+#")))
                    continue;

                if (!isJobSectionFound) {
                    // Expecting [CONFIG] as a beginning content of the file
                    //
                    string dupLine = line;
                    config_cat ccat = getConfType (rtrim (ltrim (dupLine, '['), ']'));

                    switch (ccat) {
                        case CONFIG:
                            isConfigSectionFound = true;
                            continue;

                        case JOBS:
                            isJobSectionFound = true;
                            // Store the [JOBS] section line number
                            //
                            jobSectionLinePos = conFileStream.tellg();
                            continue;

                        default:
                            if (!isConfigSectionFound)
                                throw schExcp (ERROR, "Syntax error. Unexpected configuration type " + dupLine + " at line: " + std::to_string (lineNum));
                    }
                }

                // [CONFIG] section is found, and do let us parse the parameter settings
                //
                kv.convertStr2KV (line);

                if (kv.getKey().empty() || kv.getValue().empty())
                    throw schExcp (ERROR, "Invalid entry in config file: " + line + " at line: " + std::to_string (lineNum));
                else {
                    bool isReloadable;

                    if (isValidConfParam (kv.getKey().c_str(), kv.getValue().c_str(), isReloadable) != VALID)
                        throw schExcp (ERROR, "Invalid parameter " + line + " in config file at line: " + std::to_string (lineNum));
                    else {
                        if (isReload)
                            this->setConfigParam (kv.getKey(), kv.getValue(), isReloadable);
                        else
                            this->setConfigParam (kv.getKey(), kv.getValue(), false);

                        continue;	// Found the setting as valid one, and continue to parse the remaining entries
                    }
                }
            }

        } else
            throw schExcp (ERROR, "Unable to locate the configuration file");

    } catch (exception &e) {
        conFileStream.close();
        e.what();
        return false;
    }

    conFileStream.close();
    return true;
}
/*
 * jobSectionLinePos:
 * 		Returns the [JOBS] section line position, which was stored during the [CONFIG] section parsing state.
 */
std::streamoff configParams::getJobSectionLinePos()
{
    return jobSectionLinePos;
}
/*
 * validateConfig:
 *		This method validates whether all the settings are properly populated or not.
 */
bool configParams::validateConfig()
{
    for (uint i = 0; !config_params[i][0].empty(); i++) {
        // If the given setting not found in the map, then return false from here.
        //
        if (getConfigParam (config_params[i][0]) == nullptr)
            return false;
    }

    return true;
}
/*
 * Destructor of the configParams
 */
configParams::~configParams()
{
    closeConfStream();
}
/*
 * jobConfig:
 *      Default constructor of jobConfig object.
 */
jobConfig::jobConfig (const char *file, schDbOps *ops, configParams *conParams, bool isUpd)
{
    fileLoc = file;
    conFile.open (file, ifstream::in);
    schOps = ops;
    isUpdate = isUpd;
    totalJobs = 0;
    confParams = conParams;
}
/*
 * isValidJobArg:
 *      Checks whether the specified keywords in configuration file are valid or not.
 */
bool jobConfig::isValidJobArg (const char *arg)
{
    // Return true, if we found the job file arg
    for (size_t i = 0; i < sizeof (jobFileArgs) / sizeof (*jobFileArgs); i++)
        if (strcasecmp (jobFileArgs[i].c_str(), arg) == 0)
            return true;

    return false;
}
/*
 * isValidConfParam:
 * 		Validate the given parameter and it's key value
 */
config_checks configParams::isValidConfParam (const char *key, const char *val, bool &isReloadable)
{
    isReloadable = false;

    for (uint i = 0; !config_params[i][0].empty(); i++) {
        if (strcasecmp (config_params[i][0].c_str(), key) == 0) {
            // Configuration parameter is matched.
            // Now, let us check for the value.
            //
            if (config_params[i][1] == "") {
                // Check whether the parameter can be reload without restart
                //
                if (config_params[i][4] == "y")
                    isReloadable = true;

                return VALID;	// Parameter will accept any text as a value to this key.

            } else {
                stringstream csvValues (config_params[i][1]);
                string confValue;

                while (getline (csvValues, confValue, ',')) {
                    if (strcasecmp (confValue.c_str(), val) == 0) {
                        if (config_params[i][4] == "y")
                            isReloadable = true;

                        return VALID; // Found the parameter key, value as valid.
                    }
                }

                return INVALID_VALUE;	// Parameter value is not valid.
            }
        }
    }

    return INVALID_SETTING; // Invalid parameter key.
}
/*
 * getConfType:
 * 		Return the configuration file section type
 */
config_cat configParams::getConfType (string confType)
{
    if (trim (confType) == "CONFIG")
        return CONFIG;
    else if (trim (confType) == "JOBS")
        return JOBS;
    else
        return INVALID_CAT;
}
/*
* Parsing the given job config file
*/
bool jobConfig::parseConfig (string jobid)
{
    size_t	lineNum = 1;
    bool	isSuccess = true, doSkipJob = false;
    bool 	setBegin = false, setEnd = false, isEvntJob = false;
    std::unordered_set<string> configJids;

    // Check, whether the given config file exists OR not
    //
    if (conFile.good()) {
        printMsg ("Jobs parsing is started..");

        try {
            schOps->loadJobIds();

        } catch (exception &e) {
            e.what();
            return false;
        }

        // Get the [JOBS] section position
        //
        if (confParams->getJobSectionLinePos() == -1) {
            printMsg ("Syntax error. Not found the [JOBS] section in the config file");
            return false;

        } else {
            // Set the current file pointer position to [JOBS] section
            //
            conFile.seekg (confParams->getJobSectionLinePos(), conFile.beg);
        }

        for (string line; getline (conFile, line) && isSuccess; lineNum++) {
            string key, val;
            line = trim (line);

            // Checking whether line is an empty
            //
            if (line.empty())
                continue;

            // Checking if line is a comment
            //
            if (regex_search (line, regex ("^#|[[:space:]]+#")))
                continue;

            // Checking for set "{" begin
            //
            if (line == "{") {
                // Reset the isEvntFlag before any set starts
                //
                isEvntJob = false;

                // Checking for, whether previous set has been closed OR not
                //
                if (setBegin) {
                    printMsg ("Syntax error. Unexpected character " + line + " at line: " + std::to_string (lineNum));
                    isSuccess = false;
                    break;

                } else if (setEnd) {
                    setBegin = true;
                    setEnd = false;
                    continue;

                } else {
                    setBegin = true;
                    continue;
                }
            }

            // Checking for set "}" end
            //
            else if (line == "}") {
                if (setBegin) {
                    setEnd = true;
                    setBegin = false;

                    // If the current job is skipped from processing, then
                    // do not increment the totalJobs counter.
                    //
                    if (doSkipJob == false)
                        totalJobs++;
                    else
                        // Now, re-set the doSkipJob to false. Since, we need this flag
                        // to be enabled for the further jobs.
                        //
                        doSkipJob = false;

                    if (isUpdate && jobid != "" && jobs["JOBID_" + std::to_string (totalJobs - 1)] == jobid) {
                        printMsg ("Found the given job -> " + jobid + " to update");
                        return true;
                    }

                    continue;

                } else if (setEnd) {
                    printMsg ("Syntax error. Unexpected character " + line + " at line: " + std::to_string (lineNum));
                    isSuccess = false;
                    break;
                }
            }

            if (doSkipJob == true)
                continue;

            // If setBegin, then only proceed with further parsing..
            //
            if (setBegin) {
                // Checking Regex in the line as PROPERTY  =  VALUE
                //
                regex rge ("(\\w+)\\s*(=)\\s*(.*)", std::regex_constants::icase);
                smatch strMatch;
                regex_match (line, strMatch, rge);

                // key != empty and =(assignment) != empty and val != empty
                //
                if (strMatch[1] != "" && strMatch[2] != "" && strMatch[3] != "") {
                    // Seems, we found the Key = Val matched string
                    //
                    key = strMatch[1];
                    val = strMatch[3];

                } else {
                    printMsg ("Invalid entry in config file: " + line + " at line: " + std::to_string (lineNum));
                    isSuccess = false;
                    break;
                }

                // Check whether the key is a valid argument or not
                //
                if (!isValidJobArg (key.c_str())) {
                    printMsg ("Invalid jobs config argument \"" + key +
                              "\" at line " + std::to_string (lineNum) + " in config file: " + string (fileLoc)
                             );
                    isSuccess = false;
                    break;
                }

                if (val.empty()) {
                    printMsg ("An empty value found for the config: " + key + " at line: " + std::to_string (lineNum));
                    isSuccess = false;
                    break;
                }

                // JOBCLASS
                //
                if (strcasecmp (key.c_str(), "JOBCLASS") == 0) {
                    if (val.empty())
                        throw schExcp (ERROR, "Found JOBCLASS property as an empty string at line: " + std::to_string (lineNum));
                    else {
                        // Validate the JOBCLASS values
                        //
                        if (strcasecmp (val.c_str(), "EVT") != 0 && strcasecmp (val.c_str(), "JOB") != 0)
                            throw schExcp (ERROR, "Invalid JOBCLASS " + val + " found at line: " + std::to_string (lineNum));

                        // Enable the isEvntJobFlag if the class type is event job
                        //
                        if (strcasecmp (val.c_str(), "EVT") == 0)
                            isEvntJob = true;
                    }
                }

                // If key is JOBID, then validate it's parameter values.
                // JOBID should be an integer
                //
                else if (strcasecmp (key.c_str(), "JOBID") == 0) {
                    try {
                        stol (val);

                        //Now, validate whether jobid>=1 or not.
                        //If it is not >=1, then return from here..
                        //
                        if (! (stol (val) >= 1)) {
                            printMsg ("Found jobid value " + val + " which is < 1 in the config file at line : " + std::to_string (lineNum));
                            return false;
                        }

                        if (configJids.size() == 0) {
                            configJids.insert (val);

                        } else if (configJids.find (val) != configJids.end()) {
                            printMsg ("Found duplicate jobid " + val + " in the config file at line : " + std::to_string (lineNum));
                            return false;
                        }

                    } catch (const std::invalid_argument &ia) {
                        printMsg ("Invalid argument passed to jobid " + val + " at line: " + std::to_string (lineNum));
                        isSuccess = false;
                        break;

                    } catch (const std::out_of_range &oor) {
                        printMsg ("JOBID value: " + val + " is out of range of long data type at line: " + std::to_string (lineNum));
                        isSuccess = false;
                        break;
                    }

                    // In insert mode, check the jobID with existing jobs
                    //
                    if (!isUpdate) {
                        if (schOps->isJobIdExists (val)) {
                            printMsg ("Job id ->" + val + " already exists. Skipping it..");
                            doSkipJob = true;
                            continue;
                        }

                    } else if (jobid != "") {
                        if (stol (val) != stol (jobid)) {
                            // Given Job id is not matching with this job. Hence, skipping it...
                            //
                            doSkipJob = true;
                            continue;
                        }
                    }
                }

                // If parameter is JOBNAME, then allow text value.
                //

                // If parameter is Enable, then allow only case insensitive,
                // True/False values.
                else if (strcasecmp (key.c_str(), "ENABLE") == 0) {
                    if (! ((strcasecmp (val.c_str(), "TRUE") == 0) || (strcasecmp (val.c_str(), "FALSE") == 0))) {
                        printMsg ("Invalid argument to ENABLE parameter: " + val + " at line: " + std::to_string (lineNum));
                        printMsg ("Provide one of [True/False] as an argument value.");
                        isSuccess = false;
                        break;
                    }
                }

                // JOBTYPE allows only OS/DB value
                //
                else if (strcasecmp (key.c_str(), "JOBTYPE") == 0) {
                    smatch jType;
                    regex_search (val, jType, regex ("(^OS$)|(^DB$)", std::regex_constants::icase));

                    if (!jType[1].matched && !jType[2].matched) {
                        printMsg ("Invalid job type option: " + val  + " at line: " + std::to_string (lineNum));
                        isSuccess = false;
                        break;
                    }
                }

                // JOBRUNFREQ
                //
                else if (strcasecmp (key.c_str(), "JOBRUNFREQ") == 0) {
                    // If the job is of type EVENT, then we do not need to parse it's run frequency.
                    // Also, do not store it's value into the database.
                    //
                    if (isEvntJob) {
                        LOG ("No need to parse/store the JOBRUNFREQ property for the event jobs.", DDEBUG);
                        val = "";

                    } else {
                        // Yeah, handle this job run frequency patterns
                        //
                        smatch jFMatch;
                        // Day regex
                        //
                        string jFregex = "\\s*(?:Day\\s*:\\s*(\\d+(?:\\s*,\\s*?\\d+)*|(?:\\*(?:\\/[0-9]+)*)))\\s*";
                        // Adding Month regex
                        //
                        jFregex += "(?:Month\\s*:\\s*(\\d+(?:\\s*,\\s*?\\d+)*|(?:\\*(?:\\/[0-9]+)*)))\\s*";
                        // Adding Hour regex
                        //
                        jFregex += "(?:Hour\\s*:\\s*(\\d+(?:\\s*,\\s*?\\d+)*|(?:\\*(?:\\/[0-9]+)*)))\\s*";
                        // Adding Minute regex
                        //
                        jFregex += "(?:Minute\\s*:\\s*(\\d+(?:\\s*,\\s*?\\d+)*|(?:\\*(?:\\/[0-9]+)*)))\\s*";
                        // Adding Second regex
                        //
                        jFregex += "(?:Second\\s*:\\s*(\\d+(?:\\s*,\\s*?\\d+)*|(?:\\*(?:\\/[0-9]+)*)))\\s*$";
                        regex_search (val, jFMatch, regex (jFregex, std::regex_constants::icase));

                        if (!jFMatch[1].matched) {
                            printMsg ("Invalid job run frequency: " + val  + " at line: " + std::to_string (lineNum));
                            isSuccess = false;
                            break;
                        }

                        try {
                            // Days
                            //
                            if (jFMatch[1] == "*")
                                // Seems, job is scheduled to run everyday.
                                //
                                jobs["JOBRUNFREQ_Days_" + std::to_string (totalJobs)] = "*";
                            else
                                prepareJobFreqEle (jFMatch[1], "Days", 1, 31, lineNum);

                            // Months
                            //
                            if (jFMatch[2] == "*")
                                // Seems, job is scheduled to run month.
                                //
                                jobs["JOBRUNFREQ_Months_" + std::to_string (totalJobs)] = "*";
                            else
                                prepareJobFreqEle (jFMatch[2], "Months", 1, 12, lineNum);

                            // Hours
                            //
                            if (jFMatch[3] == "*")
                                jobs["JOBRUNFREQ_Hours_" + std::to_string (totalJobs)] = "*";
                            else
                                prepareJobFreqEle (jFMatch[3], "Hours", 0, 23, lineNum);

                            // Minutes
                            //
                            if (jFMatch[4] == "*")
                                jobs["JOBRUNFREQ_Minutes_" + std::to_string (totalJobs)] = "*";
                            else
                                prepareJobFreqEle (jFMatch[4], "Minutes", 0, 59, lineNum);

                            // Second
                            //
                            if (jFMatch[5] == "*")
                                jobs["JOBRUNFREQ_Seconds_" + std::to_string (totalJobs)] = "*";
                            else
                                prepareJobFreqEle (jFMatch[5], "Seconds", 0, 59, lineNum);

                        } catch (exception &e) {
                            e.what();
                            isSuccess = false;
                            break;
                        }
                    }
                }

                // DBCONN
                //
                else if (strcasecmp (key.c_str(), "DBCONN") == 0) {
                    if (val.empty()) {
                        printMsg ("Found DBConn string as an empty string at line: " + std::to_string (lineNum));
                        isSuccess = false;
                    }
                }

                // DISABLEIFFAILCNT
                //
                else if (strcasecmp (key.c_str(), "DISABLEIFFAILCNT") == 0) {
                    if (val.empty())
                        throw schExcp (ERROR, "Found empty value for the DISABLEIFFAILCNT property at line: " + std::to_string (lineNum));
                    else {
                        int tmp;

                        if (!string2Int (val, tmp, 0))
                            throw schExcp (ERROR, "Found an invalid value " + val + " for the DISABLEIFFAILCNT property");
                    }
                }

                // PARSECMDPARAMS
                //
                else if (strcasecmp (key.c_str(), "PARSECMDPARAMS") == 0) {
                    if (val.empty())
                        throw schExcp (ERROR, "Found empty value for the PARSECMDPARAMS property at line: " + std::to_string (lineNum));
                    else {
                        if (strcasecmp (val.c_str(), "TRUE") != 0 && strcasecmp (val.c_str(), "FALSE") != 0)
                            throw schExcp (ERROR, "Found an invalid value " + val + " for the PARSECMDPARAMS property");
                    }
                }

                // RECDBRESCOLNAMES
                //
                else if (strcasecmp (key.c_str(), "RECDBRESCOLNAMES") == 0) {
                    if (val.empty())
                        throw schExcp (ERROR, "Found empty value for the RECDBRESCOLNAMES property at line: " + std::to_string (lineNum));
                    else {
                        if (strcasecmp (val.c_str(), "TRUE") != 0 && strcasecmp (val.c_str(), "FALSE") != 0)
                            throw schExcp (ERROR, "Found an invalid value " + val + " for the RECDBRESCOLNAMES property");
                    }
                }

                // JOBPASSEVNTS
                //
                else if (strcasecmp (key.c_str(), "JOBPASSEVNTS") == 0) {
                    if (!val.empty()) {
                        // Validate the integer csv Values
                        // Here while parsing the integer csv values, we do not need to specify any upper limit.
                        // Since, we are parsing the jobids rather schedule time entity values.
                        //
                        parseIntCsv (val, 1, NORANGE, lineNum);
                    }
                }

                // JOBFAILEVNTS
                //
                else if (strcasecmp (key.c_str(), "JOBFAILEVNTS") == 0) {
                    if (!val.empty()) {
                        // Validate the integer csv Values
                        //
                        parseIntCsv (val, 1, NORANGE, lineNum);
                    }
                }

                // JOBFAILIFRESULT
                //
                else if (strcasecmp (key.c_str(), "JOBFAILIFRESULT") == 0) {
                    if (val.empty())
                        throw schExcp (ERROR, "Found empty value for the JOBFAILOUTPUT property at line: " + std::to_string (lineNum));
                }

                // Everything went fine with this job.
                // Storing results, along with the set number.
                //
                jobs[key + "_" + std::to_string (totalJobs)]	= val;
            }

            // Without set begin, found something.
            //
            else {
                printMsg ("Syntax error. Expected { at line: " + std::to_string (lineNum));
                isSuccess = false;
                break;
            }
        }

        // Checking for unclosed braces
        //
        if (setBegin && !setEnd && isSuccess) {
            printMsg ("Syntax error. Reached EOF, without closing braces at line: " + std::to_string (lineNum));
            isSuccess = false;
        }

    } else {
        printMsg ("Unable to locate the given config file: " + string (fileLoc));
        isSuccess = false;
    }

    return isSuccess;
}
/*
 * pushJobs2Db:
 *      This method will push the parsed jobs into the database.
 */
bool jobConfig::pushJobs2Db (int &tJobs)
{
    unsigned int sucCnt = 0;

    try {
        printMsg ("Pushing jobs " + std::to_string (totalJobs) + " to database..");

        for (int i = 0; i < totalJobs; i++) {
            string istr = std::to_string (i);

            if (schOps->insUpdJob (
                    jobs["JOBID_" + istr],
                    jobs["JOBNAME_" + istr],
                    jobs["ENABLE_" + istr],
                    jobs["JOBTYPE_" + istr], jobs["JOBRUNFREQ_" + istr],
                    jobs["JOBRUNFREQ_Days_" + istr], jobs["JOBRUNFREQ_Months_" + istr],
                    jobs["JOBRUNFREQ_Hours_" + istr], jobs["JOBRUNFREQ_Minutes_" + istr],
                    jobs["JOBRUNFREQ_Seconds_" + istr], jobs["CMD_" + istr],
                    jobs["DBCONN_" + istr],
                    jobs["DISABLEIFFAILCNT_" + istr],
                    jobs["PARSECMDPARAMS_" + istr],
                    jobs["RECDBRESCOLNAMES_" + istr],
                    jobs["JOBCLASS_" + istr],
                    jobs["JOBPASSEVNTS_" + istr],
                    jobs["JOBFAILEVNTS_" + istr],
                    jobs["JOBFAILIFRESULT_" + istr],
                    isUpdate) == true)
                sucCnt++;
        }

        printMsg (std::to_string (sucCnt) + ((sucCnt > 1) ? " jobs" : " job") + " processed.");
        tJobs = sucCnt;
        return true;

    } catch (exception &e) {
        e.what();
        throw schExcp (ERROR, "Pushing jobs to database is failed");
    }
}
/*
 * prepareJobFreqEle:
 *      This method will prepare CSV values by validating the given configuration value.
 */
void jobConfig::prepareJobFreqEle (string val, string eletype, int from, int to, size_t lineNum)
{
    // Check if we have */n format in val
    //
    if (val.find ('/') != string::npos) {
        int configVal = stoi (val.substr (val.find ("/") + 1));

        if (configVal >= from && configVal <= to)
            jobs["JOBRUNFREQ_" + eletype + "_" + std::to_string (totalJobs)] = val;
        else
            throw schExcp (ERROR, "Found an invalid value in " + eletype + " as: " + std::to_string (configVal) + " at line: " + std::to_string (lineNum));

        return;
    }

    // Parse the comma separated values,
    // and check whether you have any invalid element values in it.
    //
    vector<int> element = parseIntCsv (val, from, to, lineNum);
    sort (element.begin(), element.end(), [eletype, lineNum] (int i, int j) {
        if (i == j)
            throw schExcp (ERROR, "Found duplicate values in " + eletype + ". Value is: " + std::to_string (i) + " at line: " + std::to_string (lineNum));

        return (i < j);
    });

    for (auto e : element) {
        jobs["JOBRUNFREQ_" + eletype + "_" + std::to_string (totalJobs)] += std::to_string (e) + ",";
    }

    // Removing tail ","
    //
    jobs["JOBRUNFREQ_" + eletype + "_" + std::to_string (totalJobs)].pop_back();
}
