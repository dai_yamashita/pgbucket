/*
 * jobQueue.cpp
 *
 *      This source file has the implementation about job queue.
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/source/jobQueue.cpp
 */
#include "../include/jobQueue.h"

// Initializing static objects
//
atomic<size_t> jobsQueue::_1hJobCnt {0};
atomic<size_t> jobsQueue::_2hJobCnt {0};
atomic<size_t> jobsQueue::_3hJobCnt {0};
atomic<size_t> jobsQueue::_gt3hJobCnt {0};
extern atomic<bool> isSigterm;

jobsQueue *jobsQueue::_1hBucket = nullptr;
jobsQueue *jobsQueue::_2hBucket = nullptr;
jobsQueue *jobsQueue::_3hBucket = nullptr;
jobsQueue *jobsQueue::_gt3hBucket = nullptr;
jobsQueue *jobsQueue::front = nullptr;
jobsQueue *jobsQueue::rear = nullptr;

int jobsQueue::jobQueueTOH = 0;
mutex jobsQueue::jobQLocker;
bool jobsQueue::isRefreshStarted = false;

hashTable::hashTable (jobClass cType)
{
    hashCounter = 0;
    jClass = cType;
    hTable = new std::unordered_map<unsigned int, jobs *>;
    isResyncInProgress = false;

    if (hTable == NULL)
        throw schExcp (ERROR, "Unable to initialize jobs hash");
    else
        LOG ("Job hash initialize is done", DDEBUG);
}


/*
 * getIsReSyncInProgress:
 *      Getter method of isResyncInProgress
 */
bool hashTable::getIsReSyncInProgress()
{
    return isResyncInProgress;
}
/*
 * setIsResyncInProgress:
 *      Setter method of isResyncInProgress
 */
void hashTable::setIsResyncInProgress (bool val)
{
    isResyncInProgress = val;
}
/*
 * incHashCounter:
 *      Method which increment the hashCounter.
 */
int hashTable::incHashCounter()
{
    return hashCounter++;
}
/*
 * getHashCounter:
 *      Getter method of hashCounter.
 */
int hashTable::getHashCounter()
{
    return hashCounter;
}

int hashTable::getJobHashTOH()
{
    return 0;
}

void hashTable::setJobHashTOH (int toh)
{
    ;
}

jobsHashTable::jobsHashTable (jobClass ctype): hashTable (ctype)
{
    jobHashTOH = 0;
}

/*
 * getJobHashTOH:
 *      Getter method of the jobHashTOH {TimeOverHead}
 */
int jobsHashTable::getJobHashTOH()
{
    return jobHashTOH;
}
/*
 * setJobHashTOH:
 *      Setter method of the jobHashTOH
 */
void jobsHashTable::setJobHashTOH (int toh)
{
    jobHashTOH = toh;
}
/*
 * insertJob:
 *      Insert job into the hash.
 */
bool hashTable::insertJob (jobs *job)
{
    LOG ("Loading job into Hash - Job(Id, Name) : ("
         + std::to_string (job->getJobID()) + ", " + job->getJobName() + ")",
         DDEBUG);
    // Check whether if we have the given job in the hash.
    // If it is, then update the hash with new job settings.
    // Otherwise, insert new job into the hash
    //
    int jPos = getJobIdPos (job->getJobID());
    LckGaurd lock (getHashLocker());

    // Given jobid not found in hash
    //
    if (jPos == -1) {
        hTable->insert (std::make_pair (getHashCounter(), job));
        job->setJobDelete (false);
        this->incHashCounter();
    }

    // Yeah, we found the job already in jobHash.
    // Now, update the job with the new settings
    //
    // TODO:
    //		Implement an "=" operator overloading for the object assignments
    //
    else {
        // Before updating the job properties,
        // Let us try to get a lock
        //
        hTable->at (jPos)->lockJob();
        hTable->at (jPos)->setJobName (job->getJobName());
        hTable->at (jPos)->setDbConnStr (job->getDbConnStr());
        hTable->at (jPos)->setJobCmd (job->getJobCmd());
        hTable->at (jPos)->setJobType (job->getJobType());
        hTable->at (jPos)->setJobStatus (job->getJobStatus());

        if (job->getJobStatus())
            hTable->at (jPos)->setJobDelete (false);
        else
            hTable->at (jPos)->setJobDelete (true);

        hTable->at (jPos)->setJobHrs (job->getJobHrs());
        hTable->at (jPos)->setJobMns (job->getJobMns());
        hTable->at (jPos)->setJobSecs (job->getJobSecs());
        hTable->at (jPos)->setJobMnsPntPos (job->getJobMnsPntPos());
        hTable->at (jPos)->setJobSecPntPos (job->getJobSecPntPos());
        hTable->at (jPos)->setJobHrsPntPos (job->getJobHrsPntPos());
        hTable->at (jPos)->setJobFailIfOutput (job->getJobFailIfOutput());
        hTable->at (jPos)->setDisableFailCnt (job->getDisableFailCnt());
        hTable->at (jPos)->setIsRecordDbResCols (job->getIsRecordDbResCols());
        hTable->at (jPos)->setIsParseCmdParams (job->getIsParseCmdParams());
        hTable->at (jPos)->setJobClass (job->getJobClass());
        hTable->at (jPos)->setJobFailEvtIds (job->getJobFailEvtIds());
        hTable->at (jPos)->setJobPassEvtIds (job->getJobPassEvtIds());
        //Unlock the lock, once we done with updating the job
        //
        hTable->at (jPos)->unlockJob();
        delete job;
    }

    return true;
}
/*
 * deleteJob:
 *      This method will disable the job in the hash rather deleting it from the hash.
 */
bool hashTable::deleteJob (size_t jobId)
{
    LOG ("Setting delete flag for the jobid : " + std::to_string (jobId), DDEBUG);
    LckGaurd lock (getHashLocker());

    for (auto item = this->hTable->begin(); item != this->hTable->end(); item++) {
        if (item->second->getJobID() == jobId) {
            //Update job's delete/status flags
            //
            item->second->setJobDelete (true);
            item->second->setJobStatus (false);
            LOG ("Delete flag is enabled for the jobid : " + std::to_string (jobId), DDEBUG);
            LOG ("Disable flag is enabled for the jobid : " + std::to_string (jobId), DDEBUG);
            return true;

        } else
            continue;
    }

    throw schExcp (ERROR, "Job id -> " + std::to_string (jobId) + " not found in the hash to enable the delete flag");
    return true;
}
/*
 * getJobIdPos:
 *      This method will get the job id position pair
 */
int hashTable::getJobIdPos (size_t jobId)
{
    int jobPos = -1;
    LckGaurd lock (getHashLocker());

    for (auto item = hTable->begin(); item != hTable->end(); item++) {
        if (item->second->getJobID() == jobId) {
            jobPos = (int) item->first;
            break;
            return jobPos;
        }
    }

    return jobPos;
}
/*
 * getJob:
 *      This method will get the job at the specified position.
 */
jobs *hashTable::getJob (size_t jobPos)
{
    try {
        LOG ("Got request to get job at position " + std::to_string (jobPos), DDEBUG);
        return hTable->at ((uint)jobPos);

    } catch (exception &e) {
        LOG ("Hash is empty at the position " + std::to_string (jobPos), ERROR);
        throw;
    }
}
/*
 * getHashLocker:
 *      This method returns the hash lock.
 */
mutex &hashTable::getHashLocker()
{
    return hashLocker;
}

jobClass hashTable::getJobClass() const
{
    return jClass;
}

/*
 * cleanJobHash:
 *      This method will clear the job hash object.
 */
void hashTable::cleanHashTable()
{
    if (getJobClass() == EVENT)
        LOG ("Clearing event hash table", DDEBUG);
    else
        LOG ("Clearing job hash table", DDEBUG);

    LckGaurd lock (getHashLocker());

    for (auto j = hTable->begin(); j != hTable->end(); j++) {
        if (j->second != nullptr) {
            if (j->second->getJobSchStatus() == RUNNING) {
                LOG ("Job id ->" + std::to_string (j->second->getJobID()) + " is still running.. Please wait until it's completion.", NOTICE);
                LOG ("Use force kill option, to terminate the daemon instantly", HINT);
            }

            j->second->tryLock();
            delete j->second;
            j->second = nullptr;
        }
    }

    hTable->clear();
    hashCounter = 0;
    LOG ("Hash table is cleared", DDEBUG);
}
/*
 * ~jobsHashTable:
 *      Default destructor of jobHashTable
 */
jobsHashTable::~jobsHashTable()
{
    ;
}
/*
 * getFullJobHash:
 *      This method returns the jobsHash member variable.
 */
std::unordered_map<unsigned int, jobs *> *hashTable::getHashTable()
{
    return hTable;
}
/*
 * printHashTable:
 *      This method print the whole job hash in pretty format, and will return that printed string.
 */
string jobsHashTable::printHashTable (bool isExtended, uint numcols)
{
    vector<string> fields;
    vector<vector<string>> tuples;

    // If job hash is empty, then return from here
    //
    if (getHashTable() == nullptr || getHashTable()->size() == 0) {
        return "jobHash is empty\n";
    }

    fields.push_back ("ID");
    fields.push_back ("Name");
    fields.push_back ("PID");
    fields.push_back ("Enabled");
    fields.push_back ("SchStatus");
    fields.push_back ("PrevRunStatus");
    fields.push_back ("StartTime");
    fields.push_back ("EndTime");
    fields.push_back ("NextRun");
    fields.push_back ("RunCount");

    for (auto j = getHashTable()->begin(); j != getHashTable()->end(); j++) {
        vector<string> row;

        if (j->second != nullptr ) {
            row.push_back (std::to_string (j->second->getJobID()));
            row.push_back (j->second->getJobName());
            row.push_back (std::to_string (j->second->getJobPid()));
            // If job status is enabled and it's job delete flag is false,
            // then print the job status as "Enable".
            //
            row.push_back ( (j->second->getJobStatus() && !j->second->isJobDeleted()) ? "Enable" : "Disable");

            switch (j->second->getJobSchStatus()) {
                case RUNNING:
                    row.push_back ("RUNNING");
                    break;

                case SCHEDULED:
                    row.push_back ("SCHEDULED");
                    break;

                case DISPATCHED:
                    row.push_back ("DISPATCHED");
                    break;

                case COMPLETED:
                    row.push_back ("COMPLETED");
                    break;

                case KILLED:
                    row.push_back ("KILLED");
                    break;

                case INITIALIZED:
                    row.push_back ("INITIALIZED");
                    break;

                case SKIPPED:
                    row.push_back ("SKIPPED");
                    break;

                case RUNNING_EVENTJOB:
                    row.push_back ("RUNNING_EVENTJOB");
                    break;

                default:
                    row.push_back ("INVALID");
                    break;
            }

            // If job run one or more times then only show it's previous run status
            //
            if (j->second->getJobRunCounter() >= 1)
                row.push_back (j->second->getPrevRunStatus() ? "Success" : "Fail");
            else
                row.push_back ("Unknown");

            time_t val = j->second->getJobStartTime();
            string ascTime;

            if (val != 0) {
                ascTime = std::asctime (std::localtime (&val));
                row.push_back (rtrim (ascTime, '\n'));

            } else
                row.push_back (" ");

            val = j->second->getJobEndTime();

            if (val != 0 ) {
                ascTime = std::asctime (std::localtime (&val));
                row.push_back (rtrim (ascTime, '\n'));

            } else
                row.push_back (" ");

            val = j->second->getJobNextRun();

            if (val != 0 ) {
                ascTime = std::asctime (std::localtime (&val));
                row.push_back (rtrim (ascTime, '\n'));

            } else
                row.push_back (" ");

            row.push_back (std::to_string (j->second->getJobRunCounter()));
            tuples.push_back (row);
        }
    }

    return printTable ("Job hash instance", tuples, fields, isExtended, numcols);
}

/*
 * printHashTable:
 *      This method print the whole event hash in pretty format, and will return that printed string.
 */
string evntsHashTable::printHashTable (bool isExtended, uint numcols)
{
    vector<string> fields;
    vector<vector<string>> tuples;

    // If job hash is empty, then return from here
    //
    if (getHashTable() == nullptr || getHashTable()->size() == 0) {
        return "jobHash is empty\n";
    }

    fields.push_back ("ID");
    fields.push_back ("Name");
    fields.push_back ("PID");
    fields.push_back ("Enabled");
    fields.push_back ("SchStatus");
    fields.push_back ("PrevRunStatus");
    fields.push_back ("StartTime");
    fields.push_back ("EndTime");
    fields.push_back ("RunCount");

    for (auto j = getHashTable()->begin(); j != getHashTable()->end(); j++) {
        vector<string> row;

        if (j->second != nullptr ) {
            row.push_back (std::to_string (j->second->getJobID()));
            row.push_back (j->second->getJobName());
            row.push_back (std::to_string (j->second->getJobPid()));
            // If job status is enabled and it's job delete flag is false,
            // then print the job status as "Enable".
            //
            row.push_back ( (j->second->getJobStatus() && !j->second->isJobDeleted()) ? "Enable" : "Disable");

            switch (j->second->getJobSchStatus()) {
                case RUNNING:
                    row.push_back ("RUNNING");
                    break;

                case SCHEDULED:
                    row.push_back ("SCHEDULED");
                    break;

                case DISPATCHED:
                    row.push_back ("DISPATCHED");
                    break;

                case COMPLETED:
                    row.push_back ("COMPLETED");
                    break;

                case KILLED:
                    row.push_back ("KILLED");
                    break;

                case INITIALIZED:
                    row.push_back ("INITIALIZED");
                    break;

                case SKIPPED:
                    row.push_back ("SKIPPED");
                    break;

                case RUNNING_EVENTJOB:
                    row.push_back ("RUNNING_EVENTJOB");
                    break;

                default:
                    row.push_back ("INVALID");
                    break;
            }

            // If job run one or more times then only show it's previous run status
            //
            if (j->second->getJobRunCounter() >= 1)
                row.push_back (j->second->getPrevRunStatus() ? "Success" : "Fail");
            else
                row.push_back ("Unknown");

            time_t val = j->second->getJobStartTime();
            string ascTime;

            if (val != 0) {
                ascTime = std::asctime (std::localtime (&val));
                row.push_back (rtrim (ascTime, '\n'));

            } else
                row.push_back (" ");

            val = j->second->getJobEndTime();

            if (val != 0 ) {
                ascTime = std::asctime (std::localtime (&val));
                row.push_back (rtrim (ascTime, '\n'));

            } else
                row.push_back (" ");

            row.push_back (std::to_string (j->second->getJobRunCounter()));
            tuples.push_back (row);
        }
    }

    return printTable ("Event hash instance", tuples, fields, isExtended, numcols);
}

/*
 * jobsQueue:
 *      Default constructor of jobsQueue object.
 */
jobsQueue::jobsQueue()
{
    next = prev = nullptr;
    jobIdPos.hashPos = 0;
    jobIdPos.jobId = 0;
    schBucket = GT_3H;
    startInSec = GT_3H / _1HOUR;
    nTimesRun = nullptr;
}

/*
 * jobsQueue:
 *      Parameterized constructor of jobsQueue object along with delegating it's default constructor.
 */
jobsQueue::jobsQueue (std::unordered_map<unsigned int, jobs *> *hTable) : jobsQueue()
{
    time_t currEpoch = getMyClockEpoch();
    LOG ("Initiating job queue", DDEBUG);

    for (auto j = hTable->begin(); j != hTable->end(); j++) {
        jobsQueue *tmpNode = nullptr;

        try {
            tmpNode = new jobsQueue;

        } catch (std::bad_alloc &ba) {
            // If tmpNode creation failure
            //
            LOG (ba.what(), ERROR);
            throw ba;
        }

        tmpNode->jobIdPos.jobId = j->second->getJobID();
        tmpNode->jobIdPos.hashPos = j->first;
        tmpNode->nTimesRun = j->second->getNTimesRun();
        // We are pushing the job into the queue for the first time.
        // So, the nTimesRun always needs be initialized with 0.
        //
        tmpNode->nTimesRun = 0;
        long nextRun = tmpNode->setJobNextRun (j->second, currEpoch);

        if (nextRun <= -1) {
            LOG ("Job id ->" + std::to_string (tmpNode->getJobId())
                 + " next run is (-ve), " + std::to_string (nextRun) + " and not dispatching it for today.", DDEBUG);
            LOG ("Might be this Job id ->" + std::to_string (tmpNode->getJobId()) + " is not configured to run today", DDEBUG);
            delete tmpNode;
            continue;
        }

        // Populating into an empty queue.
        //
        if (front == nullptr) {
            front = rear = tmpNode;
            tmpNode->next = nullptr;
            tmpNode->prev = nullptr;

        } else {
            tmpNode->prev = rear;
            rear->next = tmpNode;
            rear = tmpNode;
            rear->next = nullptr;
        }

        // Incrementing bucket counters
        //
        switch (tmpNode->schBucket) {
            case LTE_1H:
                _1hJobCnt++;
                break;

            case LTE_2H:
                _2hJobCnt++;
                break;

            case LTE_3H:
                _3hJobCnt++;
                break;

            default:
                _gt3hJobCnt++;
        }
    }

    // Sorting jobs
    //
    LOG ("Started sorting job queue", DDEBUG);
    sortFullQueue (front, rear);
    LOG ("Completed sorting job queue", DDEBUG);
    LOG ("Job queue initialization is done", DDEBUG);
}
/*
 * setJobNextRun:
 *      This method will calculate the job's next run as per the scheduled cron.
 */
long jobsQueue::setJobNextRun (jobs *job, time_t currTime)
{
    long nextRunInSec = -1;
    bool foundNextRun = false;
    time_t stime = currTime;
    time_t etime;
    LckGaurd lock (jobQLocker);

    if (job == nullptr) {
        LOG ("Unable to calculate job's next run, as it is empty", DDEBUG);
        return -1;
    }

    // Before calculating job's next run, check whether the job status is enabled or not
    //
    if (!job->getJobStatus()) {
        LOG ("Job id ->" + std::to_string (job->getJobID()) + " is disabled.", NOTICE);
        return -1;
    }

    string hrs = "", mns = "", secs = "";

    for (auto x : *job->getJobHrs()) {
        hrs += std::to_string (x) + ",";
    }

    for (auto x : *job->getJobMns()) {
        mns += std::to_string (x) + ",";
    }

    for (auto x : *job->getJobSecs()) {
        secs += std::to_string (x) + ",";
    }

    rtrim (hrs, ',');
    rtrim (mns, ',');
    rtrim (secs, ',');
    LOG ("Job id ->" + std::to_string (job->getJobID()) + " calculated hours are : " + hrs, DDEBUG);
    LOG ("Job id ->" + std::to_string (job->getJobID()) + " calculated minutes are : " + mns, DDEBUG);
    LOG ("Job id ->" + std::to_string (job->getJobID()) + " calculated seconds are : " + secs, DDEBUG);
    size_t hour = getTimeElement (HOUR), minute = getTimeElement (MINUTE), second = getTimeElement (SECOND);

    // Find job's next run
    //
    for (uint hpos = job->getJobHrsPntPos(); hpos < job->getJobHrs()->size(); hpos++, job->setJobMnsPntPos (0)) {
        // Proceed with future hours,
        // which will reduce the unnecessary calculation for the passed hours
        //
        if (! (job->getJobHrs()->at (hpos) >= (int) hour))
            continue;

        for (uint mpos = job->getJobMnsPntPos(); mpos < job->getJobMns()->size(); mpos++, job->setJobSecPntPos (0)) {
            // Skip calculation for all the passed minutes for the current hour
            //
            if ((! (job->getJobMns()->at (mpos) >= (int) minute)) && (! (job->getJobHrs()->at (hpos) >= (int) hour)))
                continue;

            for (uint spos = job->getJobSecPntPos(); spos < job->getJobSecs()->size(); spos++) {
                // Skip calculation for all the passed seconds for the current minute
                //
                if ((! (job->getJobSecs()->at (spos) >= (int) second)) &&
                    (! (job->getJobMns()->at (mpos) >= (int) minute)) && (! (job->getJobHrs()->at (hpos) >= (int) hour)))
                    continue;

                nextRunInSec = (get2DaysBeginEpoch()
                                + ((job->getJobHrs()->at (hpos) * 3600
                                    + job->getJobMns()->at (mpos) * 60
                                    + job->getJobSecs()->at (spos)))) - (currTime);

                if (nextRunInSec >= 0) {
                    // Yeah, found the job's nextRun
                    // So, update the hr, mn, ss pos
                    //
                    LOG (
                        "Calculated the jobId : "
                        + std::to_string (job->getJobID())
                        + " next run as " + std::to_string (nextRunInSec) + " seconds",
                        DDEBUG);
                    LOG ("Hours : " + std::to_string (job->getJobHrs()->at (hpos)), DDEBUG);
                    LOG ("Minutes : " + std::to_string (job->getJobMns()->at (mpos)), DDEBUG);
                    LOG ("Seconds : " + std::to_string (job->getJobSecs()->at (spos)), DDEBUG);
                    foundNextRun = true;

                    if (! (spos + 1 < job->getJobSecs()->size())) {
                        LOG ("Reached end of the secs vector. So, re-setting the secs position to 0 for this Job id ->" + std::to_string (job->getJobID()), DDEBUG);
                        job->setJobSecPntPos (0);

                        if (! (mpos + 1 < job->getJobMns()->size())) {
                            LOG ("Reached end of the mns vector. So, re-setting the mins position to 0 by incrementing the hrs position by 1 for this Job id ->" + std::to_string (job->getJobID()), DDEBUG);
                            job->setJobHrsPntPos (hpos + 1);
                            job->setJobMnsPntPos (0);

                        } else {
                            LOG ("Not reached end of the mns vector. So, incrementing it's value by 1 for this Job id ->" + std::to_string (job->getJobID()), DDEBUG);
                            job->setJobHrsPntPos (hpos);
                            // Seems, we covered all elements from secs vector.
                            // So, there is no harm to push job's minPntPos to min + 1.
                            //
                            job->setJobMnsPntPos (mpos + 1);
                        }

                    } else {
                        LOG ("Not reached end of the secs vector. So, incrementing it's value by 1 for this Job id ->" + std::to_string (job->getJobID()), DDEBUG);
                        job->setJobMnsPntPos (mpos);
                        job->setJobHrsPntPos (hpos);
                        // Yeah, got the job's next run in seconds.
                        // Now, set the job's secPntPos to spos + 1.
                        // By this way, we can avoid to re-calculate
                        // the job's next run from the same secPntPos position
                        //
                        job->setJobSecPntPos (spos + 1);
                    }
                }

                if (foundNextRun)
                    break;
            }

            if (foundNextRun)
                break;
        }

        if (foundNextRun)
            break;
    }

    etime = getMyClockEpoch();

    if (nextRunInSec < 0) {
        LOG ("Job id ->" + std::to_string (job->getJobID()) + " configured schedules are completed.", NOTICE);
        job->setJobNextRun (0);
        return nextRunInSec;
    }

    if (nextRunInSec > 0 && (nextRunInSec - (etime - stime)) < 0) {
        LOG (
            "Calculating jobid-> " + std::to_string (job->getJobID())
            + "'s next schedule takes " + std::to_string (etime - stime)
            + " seconds and it is already bypassed its schedule time. Hence, setting the job schedule time as 0.",
            NOTICE);
        nextRunInSec = 0;

        if (! (this->getIsRefreshStarted())) {
            setJobsBucket (*this, currTime + nextRunInSec, 0);
            job->setJobNextRun (currTime);

        } else {
            nextRunInSec = -1;
            job->setJobNextRun (0);
        }

        return nextRunInSec;

    } else {
        if (nextRunInSec == 0) {
            LOG ("Found the jobid-> " + std::to_string (job->getJobID()) + " next run as 0 seconds.", DDEBUG);

            if (! (this->getIsRefreshStarted())) {
                setJobsBucket (*this, currTime, 0);
                job->setJobNextRun (currTime);

            } else {
                nextRunInSec = -1;
                job->setJobNextRun (0);
            }

            return nextRunInSec;

        } else {
            nextRunInSec = nextRunInSec - (etime - stime);

            if (! (this->getIsRefreshStarted())) {
                setJobsBucket (*this, currTime + nextRunInSec, nextRunInSec);
                job->setJobNextRun (currTime + nextRunInSec);

            } else {
                nextRunInSec = -1;
                job->setJobNextRun (0);
            }
        }

        return nextRunInSec;
    }
}

/*
 * delJobFrmQueue:
 *      This method will delete the given job id from the queue.
 */
void jobsQueue::delJobFrmQueue (size_t jobId)
{
    LckGaurd lock (jobQLocker);
    jobsQueue *tmp = front;
    jobSchBuckets schBucket;

    if (front == nullptr) {
        // While daemon smooth termination, let us keep the daemon to be quite and do not print any empty jobQ messages.
        // Since, jobQ might already cleared and current running job may try to delete the jobID from the empty jobQ.
        //
        if (isSigterm)
            return;

        // What ?
        // Trying to delete a job from an empty jobQueue..
        //
        throw schExcp (ERROR, "Trying to delete job -> " + std::to_string (jobId) + " from an empty jobQ.");

    } else {
        // Find jobQueue element, with the given jobId.
        // TODO:
        //		Implement binary search which is sorted on startInSec
        //
        while (tmp) {
            if (tmp->getJobId() == jobId)
                break;

            tmp = tmp->next;
        }

        if (tmp == nullptr)
            throw schExcp (ERROR, "Job id ->" + std::to_string (jobId) + " not found to delete from the jobQ.");

        schBucket = tmp->schBucket;

        // Update Bucket counters
        //
        switch (schBucket) {
            case LTE_1H:
                _1hJobCnt = _1hJobCnt - 1;
                break;

            case LTE_2H:
                _2hJobCnt = _2hJobCnt - 1;
                break;

            case LTE_3H:
                _3hJobCnt = _3hJobCnt - 1;
                break;

            case GT_3H:
                _gt3hJobCnt = _gt3hJobCnt - 1;
                break;
        }

        LOG ("Deleting job " + std::to_string (jobId) + " from jobQ.", DDEBUG);

        if (front == rear) {
            LOG ("Only one element in jobQ, and deleting it", DDEBUG);
            delete tmp;
            front = rear = _1hBucket = _2hBucket = _3hBucket = nullptr;

        } else {
            if (tmp->next != nullptr) {
                LOG ("Seems we have more one than 1 element in jobQ.", DDEBUG);

                // Check if tmp is front, if so,
                // forward front pointer
                //
                if (tmp == front) {
                    tmp->next->prev = nullptr;
                    front = tmp->next;

                } else {
                    tmp->next->prev = tmp->prev;
                    tmp->prev->next = tmp->next;
                }

                LOG ("Its next value is " + std::to_string (tmp->next->getJobId()), DDEBUG);

            } else {
                if (front == tmp)
                    front = tmp->next;
                else {
                    tmp->prev->next = nullptr;
                    // If tmp->next =  NULL, then it's a rear pointer.
                    // So, backward rear pointer
                    //
                    rear = tmp->prev;
                }

                LOG ("Its previous value is " + std::to_string (tmp->prev->getJobId()), DDEBUG);
            }

            // Update bucket pointers as per item delete
            //
            switch (tmp->schBucket) {
                case LTE_1H:
                    if (tmp == _1hBucket) {
                        // Ah.. The item, which we delete fall into
                        // 1 hour bucket pointer. So, either forward this pointer
                        // OR set it to null
                        //
                        if (tmp->next != NULL && tmp->next->schBucket == LTE_1H)
                            _1hBucket = tmp->next;
                        else
                            _1hBucket = nullptr;
                    }

                    LOG ("After deleting jobid->" + std::to_string (jobId) + " bucket pointer position is at " +
                         (_1hBucket ? std::to_string (_1hBucket->getJobId()) : "nullptr"), DDEBUG);
                    break;

                case LTE_2H:
                    if (tmp == _2hBucket) {
                        // Forward 2 hour bucket OR set to null
                        //
                        if (tmp->next != NULL && tmp->next->schBucket == LTE_2H)
                            _2hBucket = tmp->next;
                        else
                            _2hBucket = nullptr;
                    }

                    break;

                case LTE_3H:
                    if (tmp == _3hBucket) {
                        // Forward 3 hour bucket OR set to null
                        //
                        if (tmp->next != NULL && tmp->next->schBucket == LTE_3H)
                            _3hBucket = tmp->next;
                        else
                            _3hBucket = nullptr;
                    }

                    break;

                case GT_3H:
                    if (tmp == _gt3hBucket) {
                        // Forward > 3 hour bucket OR set to null
                        //
                        if (tmp->next != NULL && tmp->next->schBucket == GT_3H)
                            _gt3hBucket = tmp->next;
                        else
                            _gt3hBucket = nullptr;
                    }

                    break;

                default:
                    throw schExcp (ERROR, "Invalid job bucket found, during bucket pointer forward");
            }

            delete tmp;
        }
    }
}
/*
 * insJob2Queue:
 *      This method insert the job id into the job queue
 */
void jobsQueue::insJob2Queue (jobIdPosPair jobId, time_t currEpoch, long startInSec)
{
    LckGaurd lock (jobQLocker);
    jobsQueue *item, **bucket;

    try {
        item = new jobsQueue;

    } catch (std::bad_alloc &ba) {
        throw schExcp (ERROR, ba.what());
    }

    item->jobIdPos = jobId;
    this->setJobsBucket (*item, currEpoch + startInSec, startInSec);
    bucket = (this->getBucketStart (item->getJobBucket()));
    //Now, do insertion sort as per buckets
    //
    LOG ("Inserting jobid->" + std::to_string (item->jobIdPos.jobId), DDEBUG);

    if (*bucket == nullptr && front == rear && front == nullptr) {
        //Ah! Seems complete jobQueue is empty.
        //And this is the first element in jobQueue
        //
        LOG ("Null bucket pointer for Jobid->" + std::to_string (item->jobIdPos.jobId), DDEBUG);
        front = rear = *bucket = item;
    }

    // Adjusting bucket pointers, as per new insertion
    //
    switch (item->schBucket) {
        case LTE_1H:
            if (_1hBucket == nullptr) {
                // 1 Hour bucket is not yet filled it seems.
                //
                _1hBucket = item;
                *bucket = _1hBucket;
            }

            LOG ("Incrementing 1 hour bucket counter", DDEBUG);
            _1hJobCnt++;
            break;

        case LTE_2H:
            if (_2hBucket == nullptr) {
                _2hBucket = item;
                *bucket = _2hBucket;
            }

            LOG ("Incrementing 2 hour bucket counter", DDEBUG);
            _2hJobCnt++;
            break;

        case LTE_3H:
            if (_3hBucket == nullptr) {
                _3hBucket = item;
                *bucket = _3hBucket;
            }

            LOG ("Incrementing 3 hour bucket counter", DDEBUG);
            _3hJobCnt++;
            break;

        case GT_3H:
            if (_gt3hBucket == nullptr) {
                _gt3hBucket = item;
                *bucket = _gt3hBucket;
            }

            LOG ("Incrementing greater than 3 hour bucket counter", DDEBUG);
            _gt3hJobCnt++;
            break;
    }

    if (front == rear) {
        if (item->schBucket <= front->schBucket || item->startInSec <= front->startInSec) {
            if (front != item) {
                item->next = front;
                front = *bucket = item;
                rear = item->next;
                item->next->prev = item;
            }

        } else {
            front->next = item;
            item->prev = front;
            rear = item;
        }

    } else if (front != item) {
        if (item == *bucket) {
            LOG ("This is the new element into the bucket", DDEBUG);
            //TODO:
            // 		Implement a helper function, which will find the item position and
            //		insert the item in the given range.
            //
            jobsQueue *tmp;

            // Find position to insert this item
            //
            for (tmp = front; tmp->next != nullptr; tmp = tmp->next) {
                if (item->schBucket >= tmp->schBucket && item->startInSec > tmp->startInSec)
                    continue;
                else
                    break;
            }

            if ((item->schBucket == tmp->schBucket && item->startInSec <= tmp->startInSec) ||
                (item->schBucket < tmp->schBucket)) {
                if (tmp == front)
                    front = item;

                if (tmp->prev != NULL) {
                    // If the element is not the front element
                    //
                    item->prev = tmp->prev;
                    tmp->prev->next = item;
                }

                tmp->prev = item;
                item->next = tmp;

            } else {
                if (tmp == rear)
                    rear = item;

                if (tmp->next != NULL) {
                    // If the element is not rear element
                    //
                    item->next = tmp->next;
                    item->next->prev = item;
                }

                item->prev = tmp;
                tmp->next = item;
            }

        } else if (item->startInSec <= (*bucket)->startInSec) {
            // This new job needs to run ahead than the,
            // current bucket pointed job.
            //
            item->next = *bucket;

            if ((*bucket)->prev) {
                (*bucket)->prev->next = item;
                item->prev = (*bucket)->prev;
            }

            (*bucket)->prev = item;

            // If bucket is equal to front, then we need to update the front to point
            // to this new item
            //
            if (*bucket == front)
                front = item;

            *bucket = item;

        } else {
            // Find the position to insert this item
            //
            jobsQueue *node = *bucket;

            for (; node->next != nullptr; node = node->next) {
                if (item->schBucket >= node->schBucket && item->startInSec > node->startInSec)
                    continue;
                else
                    break;
            }

            if ((item->schBucket == node->schBucket && item->startInSec <= node->startInSec) ||
                (item->schBucket < node->schBucket)) {
                if (front == node) {
                    front = item;
                    *bucket = item;
                }

                if (node->prev != NULL) {
                    node->prev->next = item;
                    item->prev = node->prev;
                }

                node->prev = item;
                item->next = node;

            } else {
                if (node == rear)
                    rear = item;

                if (node->next != NULL) {
                    // If the element is not rear element
                    //
                    item->next = node->next;
                    node->next->prev = item;
                }

                item->prev = node;
                node->next = item;
            }
        }
    }
}
/*
 * setJobsBucket:
 *      This method update the job's bucket which it belongs to.
 */
void jobsQueue::setJobsBucket (jobsQueue &tmpNode, time_t nextRun, time_t noSecDiff)
{
    tmpNode.startInSec = nextRun;

    if (noSecDiff <= LTE_1H)
        tmpNode.schBucket = LTE_1H;
    else if (noSecDiff <= LTE_2H)
        tmpNode.schBucket = LTE_2H;
    else if (noSecDiff <= LTE_3H)
        tmpNode.schBucket = LTE_3H;
    else
        tmpNode.schBucket = GT_3H;
}
/*
 * getJob id ->
 *      This method return the jobId from the jobIdPos object.
 */
size_t jobsQueue::getJobId()
{
    return jobIdPos.jobId;
}
/*
 * printJobQueue:
 *      This method prints the job queue in pretty format, and return the printed string.
 */
string jobsQueue::printJobQueue (bool isExtended, uint numcols)
{
    LckGaurd lock (jobQLocker);
    jobsQueue *tmp = this->front;
    jobSchBuckets tmpBucket;
    time_t currentEpoch = getMyClockEpoch();
    bool is1H = false, is2H = false, is3H = false, isGt3h = false;
    vector<string> fields;
    vector<vector<string>> tuples;
    fields.push_back ("Bucket");
    fields.push_back ("ID");
    fields.push_back ("NextRun(sec)");

    if (tmp != nullptr) {
        while (tmp) {
            vector<string> row;
            tmpBucket = tmp->getJobSchBucket();

            switch (tmpBucket) {
                case LTE_1H:
                    if (!is1H) {
                        row.push_back ("1Hour");
                        is1H = true;

                    } else
                        row.push_back (" ");

                    break;

                case LTE_2H:
                    if (!is2H) {
                        row.push_back ("2Hour");
                        is2H = true;

                    } else
                        row.push_back (" ");

                    break;

                case LTE_3H:
                    if (!is3H) {
                        row.push_back ("3Hour");
                        is3H = true;

                    } else
                        row.push_back (" ");

                    break;

                case GT_3H:
                    if (!isGt3h) {
                        row.push_back ("GT 3Hour");
                        isGt3h = true;

                    } else
                        row.push_back (" ");

                    break;
            }

            row.push_back (std::to_string (tmp->getJobId()));
            row.push_back (std::to_string (tmp->getJobsNextRun() - currentEpoch));
            tuples.push_back (row);
            tmp = tmp->next;
        }

        return printTable ("JobQ instance", tuples, fields, isExtended, numcols);

    } else {
        LOG ("JobQ is empty to print.", DDEBUG);
        return "JobQ is empty\n";
    }
}
/*
 * getSchHoursCount:
 *      This method returns the bucket counters.
 */
size_t jobsQueue::getSchHoursCount (jobSchBuckets schBucket)
{
    switch (schBucket) {
        case LTE_1H:
            return jobsQueue::_1hJobCnt;

        case LTE_2H:
            return jobsQueue::_2hJobCnt;

        case LTE_3H:
            return jobsQueue::_3hJobCnt;

        case GT_3H:
            return jobsQueue::_gt3hJobCnt;

        default:
            return 0;
    }
}
/*
 * doUpdBuckCounters:
 *      This method updates the bucket counters.
 *      Usually, we call this function while updating the buckets, after we spend 3600 seconds.
*/

void jobsQueue::doUpdBuckCounters()
{
    jobsQueue::_1hJobCnt = jobsQueue::_2hJobCnt = jobsQueue::_3hJobCnt = jobsQueue::_gt3hJobCnt = 0;

    // Let us traverse through the queue, and update the bucket counters
    //
    for (jobsQueue *tmp = this->front; tmp; tmp = tmp->next) {
        switch (tmp->schBucket) {
            case LTE_1H:
                jobsQueue::_1hJobCnt++;
                break;

            case LTE_2H:
                jobsQueue::_2hJobCnt++;
                break;

            case LTE_3H:
                jobsQueue::_3hJobCnt++;
                break;

            case GT_3H:
                jobsQueue::_gt3hJobCnt++;
                break;
        }
    }
}
/*
 * getJobQueueTOH:
 *      Getter method of jobQueueTOH {TimeOverHead}
 */
int jobsQueue::getJobQueueTOH()
{
    return jobQueueTOH;
}
/*
 * setJobQueueTOH:
 *      Setter method of jobQueueTOH
 */
void jobsQueue::setJobQueueTOH (int toh)
{
    jobQueueTOH = toh;
}
/*
 * sortFullQueue:
 *      This method will sort the whole job queue
 */
void jobsQueue::sortFullQueue (jobsQueue *low, jobsQueue *high)
{
    jobsQueue *pivot, *i, *j, *temp;

    if ((low && high) && (low != high)) {
        pivot = low;
        i = low;
        j = high;
        temp = new jobsQueue();

        while (j && j->next != i && i != j) {
            while (i && (i->startInSec <= pivot->startInSec) && (i != high)) {
                i = i->next;
            }

            while (j && (j->startInSec > pivot->startInSec)) {
                j = j->prev;
            }

            if ( (i && j) && j->next != i && i != j) {
                *temp = *i;
                *i = *j;
                *j = *temp;
            }
        }

        *temp = *pivot;

        if (j) {
            *pivot = *j;
            *j = *temp;
        }

        delete temp;
        sortFullQueue (low, (low == j) ? nullptr : (j ? j->prev : nullptr));
        sortFullQueue ((high == j) ? nullptr : (j ? j->next : nullptr), high);
    }
}
/*
 * doUpdBuckPntrs:
 *      This method will update the bucket pointers by pointing the job queue items.
 */
void jobsQueue::doUpdBuckPntrs()
{
    LckGaurd lock (jobQLocker);
    jobsQueue *tmp = this->front;

    // Avoid traversing, if we already set bucket pointers.
    //
    for (; (tmp && (!_1hBucket || !_2hBucket || !_3hBucket || !_gt3hBucket)); tmp = tmp->next) {
        if (tmp->schBucket == LTE_1H && _1hBucket)
            continue;
        else if (tmp->schBucket == LTE_2H && _2hBucket)
            continue;
        else if (tmp->schBucket == LTE_3H && _3hBucket)
            continue;
        else if (tmp->schBucket == GT_3H && _gt3hBucket)
            continue;

        switch (tmp->schBucket) {
            case LTE_1H:
                _1hBucket = tmp;
                break;

            case LTE_2H:
                _2hBucket = tmp;
                break;

            case LTE_3H:
                _3hBucket = tmp;
                break;

            case GT_3H:
                _gt3hBucket = tmp;
        }
    }
}
/*
 * operator=
 *      This method is overloading the operator = to assign objects.
 */
jobsQueue &jobsQueue::operator= (const jobsQueue &rhs)
{
    // Check for the self assignment operation
    //
    if (&rhs == this)
        return *this;

    jobIdPos.jobId = rhs.jobIdPos.jobId;
    jobIdPos.hashPos = rhs.jobIdPos.hashPos;
    schBucket = rhs.schBucket;
    startInSec = rhs.startInSec;
    return *this;
}
/*
 * getFront:
 *      This is a getter method of front member variable.
 */
jobsQueue *jobsQueue::getFront()
{
    return front;
}
/*
 * getBucketStart:
 *      This method returns the bucket pointing element.
 */
jobsQueue **jobsQueue::getBucketStart (jobSchBuckets b)
{
    switch (b) {
        case LTE_1H :
            return &_1hBucket;

        case LTE_2H :
            return &_2hBucket;

        case LTE_3H :
            return &_3hBucket;

        case GT_3H    :
            return &_gt3hBucket;

        default:
            return nullptr;
    }
}
/*
 * getJobBucket:
 *      This is a getter function of schBucket member variable.
 */
jobSchBuckets jobsQueue::getJobBucket()
{
    return schBucket;
}
/*
 * getJids2Dispatch:
 *      This method returns the list of the job ids, which are ready to dispatch.
 */
vector<jobIdPosPair> *jobsQueue::getJids2Dispatch (time_t runEpoch, long nSecs)
{
    LckGaurd lock (jobQLocker);
    vector<jobIdPosPair> *jIds = new vector<jobIdPosPair>();
    jobsQueue *tmp = this->_1hBucket;

    if (tmp == nullptr) {
        LOG ("1 hour bucket is empty", DDEBUG);

    } else {
        while (tmp) {
            // Check whether we reached 2 hour bucket pointer.
            // If so, let us break the loop and return the collected job ids.
            //
            if (tmp == this->_2hBucket)
                break;

            if ((tmp->startInSec - nSecs) <= runEpoch) {
                jIds->push_back (tmp->jobIdPos);
                LOG ("Job start in sec : " + std::to_string (tmp->startInSec - nSecs) + " runEpoch: " + std::to_string (runEpoch),
                     DDEBUG);
            }

            if (tmp == tmp->next) {
                // XXX: It's a bug
                // Double check if there is a node points to itself.
                //
                LOG ("Job: " + std::to_string (tmp->jobIdPos.jobId) + "is pointed to it self, which is a bug", WARNING);
                break;
            }

            tmp = tmp->next;
        }
    }

    return jIds;
}
/*
 * getJobsNextRun:
 *      This is getter method of startInSec member variable.
 */
long jobsQueue::getJobsNextRun()
{
    return startInSec;
}
/*
 * getNext:
 *      This is getter method of next member variable.
 */
jobsQueue *jobsQueue::getNext()
{
    return next;
}
/*
 * setJobSchBucket:
 *      This is setter method of schBucket member variable.
 */
void jobsQueue::setJobSchBucket (jobSchBuckets val)
{
    schBucket = val;
}
/*
 * getJobSchBucket:
 *      This is getter method of schBucket member variable.
 */
jobSchBuckets jobsQueue::getJobSchBucket()
{
    return schBucket;
}
/*
 * cleanJobQ:
 *      This method will clean the whole job queue.
 */
void jobsQueue::cleanJobQ()
{
    LckGaurd lock (jobQLocker);
    jobsQueue *tmp = front;
    jobsQueue *node = tmp;

    for (tmp = front; node; tmp = node) {
        node = tmp->next;
        delete tmp;
    }

    // Re-setting jobQ pointers
    //
    front = rear = _1hBucket = _2hBucket = _3hBucket = _gt3hBucket = nullptr;
    // Re-setting jobQ pointer counts
    //
    _1hJobCnt = _2hJobCnt = _3hJobCnt = _gt3hJobCnt = 0;
}
/*
 * setIsRefreshStarted:
 *      This is setter method of isRefreshStarted member variable.
 */
void jobsQueue::setIsRefreshStarted (bool val)
{
    isRefreshStarted = val;
}
/*
 * getIsRefreshStarted:
 *      This is a getter method of isRefreshStarted member variable.
 */
bool jobsQueue::getIsRefreshStarted()
{
    return isRefreshStarted;
}

// NEW CODE
//
hashTable::~hashTable()
{
    if (hTable!=nullptr)
    	delete hTable;
}

string hashTable::printHashTable (bool, uint)
{
    return "";
}

evntsHashTable::evntsHashTable (jobClass cType) : hashTable (cType)
{
    ;
}

evntsHashTable::~evntsHashTable()
{
    ;
}
