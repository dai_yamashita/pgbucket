/*
 * utilities.cpp
 *
 *      This source file has all the utility functions for the pgBucket
 *
 * Author:
 *      Dinesh Kumar dineshkumar02@gmail.com
 *
 * LOCATION
 *		pgBucket/source/utils/utilities.cpp
 */
#include "../../include/logger.h"
#include "../../include/jobQueue.h"
#include "../../include/utils/utils.h"

constexpr auto myName = "pgBucket";

/*
 * ltrim:
 *      This method trim the character from left.
 */
string &ltrim(string &s, char ch)
{
    s.erase(s.begin(), find_if(s.begin(), s.end(), [ch](int c) {
        return (int)ch != c;
    }));
    return s;
}
/*
 * rtrim:
 *      This method trim the character from right.
 */
string &rtrim(string &s, char ch)
{
    s.erase(find_if(s.rbegin(), s.rend(), [ch](int c) {
        return (int)ch != c;
    }).base(), s.end());
    return s;
}
/*
 * trim:
 *      This method trim the character on both sides.
 */
string &trim(string &s, char ch)
{
    return ltrim(rtrim(s, ch), ch);
}
/*
 * print_version:
 *      This method prints the pgBucket version.
 */
void print_version()
{
    std::cout << myName << " version " << PGBUCKET_VERSION << std::endl;
}
/*
 * print_usage:
 *      This method print the pgBucket help.
 */
void print_usage()
{
    std::cout << std::endl;
    std::cout << myName << " usage is as below\n\n"
              << "\t -h --help            Display this message\n"
              << "\t -v --version         Display version details\n"
              << "\t -I --init            Initialize catalog tables\n"
              << "\t -O --drop            Drop catalog tables\n"
              << "\t -f --configfile      Jobs configuration file\n"
              << "\t -D --startdaemon     Start pgBucket daemon\n"
              << "\t                           F   -> Foreground mode\n"
              << "\t                           B   -> Background mode\n"
              << "\t -Q --quitdaemon      Quit pgBucket daemon\n"
              << "\t                           n   -> Normal\n"
              << "\t                           f   -> Force\n"
              << "\t -o --reload          Reload the configuration settings\n"
              << "\t -R --refresh         Reload buckets\n"
              << "\t -S --status          Jobs status\n"
              << "\t                           A   -> All Success/Running/Failed\n"
              << "\t                           S   -> Success\n"
              << "\t                           E   -> Enabled\n"
              << "\t                           D   -> Disable\n"
              << "\t                           F   -> Failed\n"
              << "\t                           R   -> Running\n"
              << "\t -n --now             Daemon instant actions\n"
              << "\t                           RN  -> Run a job\n"
              << "\t                           RF  -> Run a job forcefully\n"
              << "\t                           E   -> Enable a job\n"
              << "\t                           D   -> Disable a job\n"
              << "\t                           SN  -> Stop a job with SIGTERM\n"
              << "\t                           SF  -> Stop a job with SIGKILL\n"
              << "\t                           PQ  -> Print job queue instance\n"
              << "\t                           PH  -> Print job hash instance\n"
              << "\t                           PCP -> Print connection pool state\n"
              << "\t                           SKN -> Skip job's next run\n"
              << "\t -s --serialid        Job result for the serial id\n"
              << "\t -L --limit           Number of jobs (Default 10)\n"
              << "\t                           -1  -> Limit all\n"
              << "\t -e --extended        Extended table print mode\n"
              << "\t -i --insert          Insert jobs config entries\n"
              << "\t -u --update          Update jobs config entries\n"
              << "\t -x --delete          Delete given job\n"
              << "\t -j --jobid           Jobid for the specific job maintenance\n";
    std::cout << std::endl;
}
/*
 * parse_log_levels:
 *      This method parse the give log levels.
 */
string parse_log_levels(logLevels loglevel)
{
    switch (loglevel) {
        case DDEBUG:
            return "DEBUG";

        case DETAIL:
            return "DETAIL";

        case HINT:
            return "HINT";

        case WARNING:
            return "WARNING";

        case ERROR:
            return "ERROR";

        case NOTICE:
            return "NOTICE";
    }

    return NULL;
}
/*
 * checkEnv:
 *      This method validates the OS environment variables.
 */
bool checkEnv(const char *envVar, string &val)
{
    if (getenv(envVar) == NULL) {
        printMsg("Environment variable " + string(envVar) + " is not defined");
        printMsg("Export this environment variable, and then re-run");
        return false;

    } else {
        val = string(getenv(envVar));

        if (trim(val, ' ').empty()) {
            printMsg("Environment variable " + string(envVar) + " value should not be empty");
            return false;
        }

        return true;
    }
}
/*
 * getTimeElement:
 *      This method returns an element value from the local timestamp.
 */
size_t getTimeElement(timeelement ele)
{
    time_t locTime = getMyClockEpoch();
    struct tm *jobTime = localtime(&locTime);

    switch (ele) {
        case YEAR:
            return jobTime->tm_year + 1900;

        case MONTH:
            return jobTime->tm_mon + 1;

        case DAY:
            return jobTime->tm_mday;

        case HOUR:
            return jobTime->tm_hour;

        case MINUTE:
            return jobTime->tm_min;

        case SECOND:
            return jobTime->tm_sec;

        default:
            return 0;
    }
}
/*
 * toQuote:
 *      This method add quotes to the given string.
 */
string &toQuote(string &s)
{
    s = "'" + s + "'";
    return s;
}
/*
 * printMsg:
 *      This method prints the given message on console.
 */
void printMsg(string msg, bool logit)
{
    //Doing left alignment with "4" spaces
    //
    std::cout << std::setfill(' ') << std::right << std::setw((int)msg.length() + 4);
    std::cout << "* " + msg << std::endl;

    if (logit)
        LOG(msg, NOTICE);
}
/*
 * parseIntCsv:
 *      This method parse the CSV of integers and return the list of integers.
 */
vector<int> parseIntCsv(string value, int from, int to, size_t lineNum)
{
    std::istringstream iCsvVals(value);
    vector<int> result;
    int ival;

    do {
        iCsvVals >> ival;

        // Check if it fails at ","
        //
        if (iCsvVals.fail()) {
            iCsvVals.clear(); // Clears bad bit
            iCsvVals.ignore();

        } else {
            if (to == NORANGE)
                result.push_back(ival);
            else if (ival >= from && ival <= to)
                result.push_back(ival);
            else
                throw schExcp(ERROR, "Invalid configuration value: " + std::to_string(ival) + " at line: " + std::to_string(lineNum));
        }
    } while (!iCsvVals.eof());

    return result;
}
/*
 * get2DaysBeginEpoch:
 *      This method returns today's starting epoch value.
 */
time_t get2DaysBeginEpoch()
{
    return dayBeginEpoch;
}
/*
 * set2DayBeginEpoch:
 *      This method sets the today's starting epoch value.
 */
void set2DayBeginEpoch()
{
    time_t locTime = time(0);
    struct tm *time = localtime(&locTime);
    dayBeginEpoch = locTime	- (
                        time->tm_hour * 3600 // Multiply 3600 sec for the no.of hours
                        +
                        time->tm_min * 60
                        +
                        time->tm_sec
                    );
}
/*
 * printTable:
 *      This method prints the given records in table format.
 */
string printTable(string tableHeader, vector<vector<string>> &recs, vector<string> &fields, bool isExtended, uint nocols)
{
    int totalLineLen = 0;
    stringstream result;

    // Check if there are any records to print.
    // If not, return from here.
    //
    if (recs.size() == 0) {
        printMsg("found 0 results for " + tableHeader);
        return result.str();
    }

    if (isExtended) {
        vector<size_t> allColWidths(recs.size());
        vector<size_t> colWidths(nocols);
        // Calculate the maximum field name width.
        //
        size_t maxFieldSize = 0;

        for (uint i = 0; i < fields.size(); i++) {
            if (fields.at(i).length() >= maxFieldSize)
                maxFieldSize = fields.at(i).length();
        }

        // Calculate the maximum column value width.
        //
        size_t row = 0, column = 0;
        size_t length = 0;

        // Traverse each records, and store the max width for each column.
        //
        for (auto rec = recs.begin(); rec != recs.end() ; rec++, row++) {
            for (auto tuple = rec->begin(); tuple != rec->end(); tuple++) {
                if (tuple->length() >= allColWidths.at(row))
                    allColWidths.at(row) = tuple->length();

                if ((uint)row > (nocols - 1)) {
                    if ( allColWidths.at(row) >= colWidths.at(row % nocols) )
                        colWidths.at(row % nocols) = allColWidths.at(row);

                } else {
                    colWidths.at(row) = allColWidths.at(row);
                }
            }
        }

        length += maxFieldSize;

        for (auto w = colWidths.begin(); w != colWidths.end(); w++) {
            // Here +3 defines, we are adding a single space and pipe and then single space between each column value.
            //
            length += *w + 3;
        }

        // Printing table header in the middle of the table
        //
        if (!(tableHeader.length() > length)) {
            result.width((length + tableHeader.length()) / 2);
            result << std::right << tableHeader << std::endl;

        } else {
            result << tableHeader << std::endl;
        }

        result.width(length);
        result.fill('-');
        result << "" << std::endl;
        // Reset line filling
        //
        result.fill(' ');
        // Print the actual data
        //
        size_t processedRows;

        for (processedRows = 0; processedRows < recs.size();) {
            for (column = 0; column < recs.begin()->size(); column++) {

                // Printing each column name
                //
                result.width(maxFieldSize);
                result << std::left << fields.at(column) << " | ";

                // Printing each column value
                //
                uint i = 0;

                for (i = 0; i < nocols && (i + processedRows) < recs.size(); i++) {
                    result.width(colWidths.at(i));
                    result << std::left << recs.at(i + processedRows).at(column);

                    // Print the column delimiter if we have an another field to print
                    //
                    if (i < nocols - 1 && (i + 1 + processedRows) < recs.size())
                        result  << " | ";
                }

                result << std::endl;
            }

            // Let us update the number of columns we processed
            //
            processedRows += nocols;
            // Printing header margin line after printing each "n" records
            //
            result.width(length);
            result.fill('-');
            result << "" << std::endl;
            // Reset line filling
            //
            result.fill(' ');
        }

        result << "(rows " << recs.size() << ")" << std::endl;

        return result.str();
    }

    else {
        vector<size_t> widths(fields.size());

        // Calculating the max width for each field
        //
        for (auto rec = recs.begin(); rec != recs.end(); rec++) {
            for (uint i = 0; i < fields.size(); i++) {
                size_t len = rec->at(i).length();

                if ( (widths[i] == 0 || widths[i] <= len) && len >= fields.at(i).length() )
                    widths.at(i) = len;
                else if (widths[i] > len)
                    continue;
                else
                    widths.at(i) = (size_t) fields.at(i).length();
            }
        }

        unsigned int length = 0;

        for (auto &w : widths) {
            // Here +3 defines, we are adding a single space and pipe and then single space between each column.
            // That is, [COLUMN NAME] | [ANOTHER COLUMN NAME]
            //
            length += w + 3;
        }

        // Printing table header in the middle of the table
        //
        if (!(tableHeader.length() > length)) {
            result.width((length + tableHeader.length()) / 2);
            result << std::right << tableHeader << std::endl;

        } else {
            result << tableHeader << std::endl;
        }

        // Printing Header
        //
        for (unsigned int i = 0; i < fields.size(); i++) {
            // Setting width
            result.width(widths.at(i));
            result << std::left << fields.at(i) << " | ";
            // Here 3 belong to the length of " | "
            totalLineLen += widths.at(i) + 3;
        }

        result << std::endl;
        // Printing header margin line
        //
        result.width(totalLineLen - 1);
        result.fill('-');
        result << "" << std::endl;
        // Resetting fill
        //
        result.fill(' ');

        // Printing actual records
        //
        for (auto rec = recs.begin(); rec != recs.end(); rec++) {
            for (unsigned int i = 0; i < fields.size(); i++) {
                result.width(widths.at(i));
                result << std::left << rec->at(i) << " | ";
            }

            result << std::endl;
        }

        result << "(" << recs.size() << " rows)" << std::endl;
        return result.str();
    }
}
/*
 * raiseRefreshSignal:
 *      This method initiates the whole instance refresh, when day begins.
 */
void raiseRefreshSignal(pthread_t threadID)
{
    if (!disableSignals()) {
        LOG("Unable to disable the maintenance signals to the pgBucket's instance refresh process.", ERROR);
        return;
    }

    while (1) {
        // If current day beginning epoch is equal to current clock epoch,
        // then invoke SIGUSR2 signal.
        //
        if (getMyClockEpoch() == get2DaysBeginEpoch() + DAYSECS) {
            set2DayBeginEpoch();
            // Invoking a day change signal, which will reload the
            // job/event hash and job queue instances
            //
#ifdef _GNU_SOURCE
            LOG("Let us do refresh the instance for the brand new day. threadID: " + std::to_string(threadID), NOTICE);
#endif
            pthread_kill(threadID, SIGALRM);
        }

        // TODO:
        // 		What happens if any date/time change happens in the system during the day light savings ?
        //
        sleep(uint(get2DaysBeginEpoch() + DAYSECS - getMyClockEpoch()));
    }
}
/*
 * getMyClockEpoch:
 *      This method returns the current clock time value
 */
time_t getMyClockEpoch()
{
    myClock = time(0);
    return myClock;
}
/*
 * isPgBucketRunning:
 *      This method validate the pgBucket instance status
 */
int isPgBucketRunning()
{
    int pid;
    FILE *fp = fopen((pidDir + "/pgBucket.pid").c_str(), "r");

    if (fp == NULL) {
        return 0;

    } else {
        fseek(fp, 0, SEEK_END);

        // If pgBucket.pid file exists and if it is empty then also return 0
        //
        if (ftell(fp) == 0) {
            fclose(fp);
            return 0;

        } else
            fseek(fp, 0, SEEK_SET);

        //FIXME:
        // Change the pid data type to `long int` from `int`.

        // Get pid from the file
        //
        if (fscanf(fp, "%d", &pid) == 1) {
            fclose(fp);

            if (kill(pid, 0) == 0)
                return pid;
            else
                return 0;

        } else {
            fclose(fp);
            return 0;
        }
    }
}
/*
 * string2Int:
 *      This method converts the string into integer
 */
bool string2Int(const string &input, int &res, int minValue)
{
    try {
        res = stoi(input);

        if (res < minValue)
            throw schExcp(ERROR, "Unable to proceed with this number " + input);

    } catch (const std::invalid_argument &ia) {
        printMsg("Unable to convert string '" + input + "' to int");
        return false;

    } catch (const std::out_of_range &oor) {
        printMsg("Out of range, in converting string '" + input + "' to int");
        return false;

    } catch (exception &e) {
        e.what();
        return false;
    }

    return true;
}
/*
 * string2Long:
 *      This method converts the string into long
 */
bool string2Long(string &input, long &res)
{
    string::size_type stype;

    try {
        res = stol(input, &stype);

    } catch (const std::invalid_argument &ia) {
        throw schExcp(ERROR, "Unable to convert string '" + input + "' to long");
        return false;

    } catch (const std::out_of_range &oor) {
        throw schExcp(ERROR, "Out of range, in converting string '" + input + "' to long");
        return false;
    }

    return true;
}
/*
 * processSockResponse:
 *      This method process the received message from the socket.
 */
bool processSockResponse(string &msg)
{
    stringstream input(msg);
    string output;
    static bool isTablePrint = false;

    while (getline(input, output, '\n')) {
        if (output == "TABLE_START") {
            isTablePrint = true;
            continue;
        }

        if (output == "TABLE_END") {
            isTablePrint = false;
            continue;
        }

        if (output == "Bye")
            return true;

        if (isTablePrint) {
            std::cout << output;

            // While processing the socket response, we may get partial table information from daemon.
            // In that case, skip printing the new line.
            //
            if (!input.eof())
                std::cout << std::endl;

            std::cout.flush();

        } else
            printMsg(output);
    }

    return false;
}
/*
 * killPid:
 *      This method kill the process with the given signal.
 */
int killPid(int pid, int signal)
{
    int status = kill(pid, signal);

    if (status < 0)
        throw schExcp(ERROR, "Failed to send signal(" + std::to_string(signal) + ") to pid(" + std::to_string(pid) + "). Error: " + string(strerror(errno)));
    else
        return status;
}
/*
 * lockFile:
 * 		This method locks the fd with the given lock type
 */
int lockFile(int fd, int mode)
{
    struct flock lckFile;
    // File lock properties
    //
    lckFile.l_type = mode;
    lckFile.l_whence = SEEK_SET;
    lckFile.l_start = 0;
    lckFile.l_len = 0;
    lckFile.l_pid = 0;


    int lckStatus = getLckFileStatus(fd, lckFile);

    if (lckStatus == 1) {
        // Trying to hold a lock
        //
        if (fcntl(fd, F_SETLK, &lckFile) == -1)
            return -1;								// Unable to hold a lock
        else
            return 1;								// Acquired lock

    } else if (lckStatus != -1) {
        // Lock is already acquired, hence returning the pid of the process
        //
        return lckStatus;
    }

    return -1;
}
/*
 * unlockFile:
 * 		This method unlocks the given fd
 */
int unlockFile(int fd)
{
    struct flock lckFile;
    // File lock properties
    //
    lckFile.l_type = F_UNLCK;
    lckFile.l_whence = SEEK_SET;
    lckFile.l_start = 0;
    lckFile.l_len = 0;

    // Trying to release the lock
    //
    if (fcntl(fd, F_SETLK, &lckFile) == -1)
        return -1;								// Unable to release the lock

    return 1;									// Able to release the lock
}
/*
 * getLckFileStatus:
 * 		This method returns the file lock status
 */
int getLckFileStatus(int fd, struct flock lckFile)
{

    // Trying to get the file lock status
    //
    if (fcntl(fd, F_GETLK, &lckFile) == -1)
        return -1;								// Unable to read the lock status
    else if (lckFile.l_type == F_UNLCK)
        return 1;								// Lock is available
    else
        return lckFile.l_pid;					// Return the pid of the process, which acquired the lock
}

/*
 * disableSignalHandlers:
 *
 *      This method blocks the specific signals for the threads.
 *      Since, we do not need each job thread to response to each signal.
 */
bool disableSignals()
{
    sigset_t signal_set;
    sigemptyset(&signal_set);
    sigaddset(&signal_set, SIGINT);
    sigaddset(&signal_set, SIGTERM);
    sigaddset(&signal_set, SIGABRT);
    sigaddset(&signal_set, SIGFPE);
    sigaddset(&signal_set, SIGSEGV);
    sigaddset(&signal_set, SIGILL);
    sigaddset(&signal_set, SIGUSR2);
    sigaddset(&signal_set, SIGUSR1);
    sigaddset(&signal_set, SIGHUP);
    sigaddset(&signal_set, SIGALRM);

    if (pthread_sigmask(SIG_BLOCK, &signal_set, NULL) != 0) {
        return false;
    }

    return true;
}

/*
 * enableSignalHandlers:
 *
 *      This method unblocks the specific signals for the threads.
 *      Since, we do need only worker process to response to each signal.
 */
bool enableSignals()
{
    sigset_t signal_set;
    sigemptyset(&signal_set);
    sigaddset(&signal_set, SIGINT);
    sigaddset(&signal_set, SIGTERM);
    sigaddset(&signal_set, SIGUSR2);
    sigaddset(&signal_set, SIGUSR1);
    sigaddset(&signal_set, SIGHUP);
    sigaddset(&signal_set, SIGABRT);
    sigaddset(&signal_set, SIGFPE);
    sigaddset(&signal_set, SIGSEGV);
    sigaddset(&signal_set, SIGILL);
    sigaddset(&signal_set, SIGALRM);

    if (pthread_sigmask(SIG_UNBLOCK, &signal_set, NULL) != 0) {
        return false;
    }

    return true;
}

void PQClearNullptr(PGresult *res)
{
    if (res != nullptr) {
        PQclear(res);
        res = nullptr;
    }
}
